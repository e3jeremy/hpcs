<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\UpdateOrderStatus::class,
        Commands\FetchConversionCake::class,
        Commands\UpdateUserConversions::class,
        Commands\SendPendingLeads::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->command('offers:update')
//            ->daily();

        $schedule->command('conversions:cake')
            ->everyFiveMinutes();

        $schedule->command('send:pending-leads')
            ->hourly();
    }

}
