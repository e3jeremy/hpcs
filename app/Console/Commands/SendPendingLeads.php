<?php

namespace App\Console\Commands;

use App\HPCS\Entities\CoregLeads;
use App\HPCS\Entities\User;
use App\Jobs\SendLeadsToLeadReactor;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Bus;

class SendPendingLeads extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:pending-leads';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send pending leads that has not been triggered from job or got an invalid response from the server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::notice('Pending Leads checking ->' . Carbon::now());

        $pendings = CoregLeads::select('user_id')->where('status',1)->where('created_at','<',Carbon::now()->subMinutes(30))->groupBy('user_id')->get();


        foreach ($pendings as $pending)
        {
            $user = User::find($pending->user_id);
            Bus::dispatch((new SendLeadsToLeadReactor($user))->delay(1));
            \Log::notice('send pending leads execute of user ID = '.$pending->user_id);
        }
    }
}
