<?php

namespace App\Console\Commands;

use App\HPCS\Entities\Offer;
use App\HPCS\Entities\User;
use App\HPCS\Entities\UserConversion;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class FetchConversionCake extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'conversions:cake';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pull conversions from cake';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->offers = Offer::get();

        parent::__construct();
    }


    private function parseXmlResponse($response) //response convert into object
    {
        return json_decode(json_encode(simplexml_load_string($response->getBody()->getContents())));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */


    public function handle()
    {
        $start_date = Carbon::today()->subDay(2)->format('m/d/Y');
        $end_date = Carbon::today()->addDay(1)->format('m/d/Y');

        $params = [
            'api_key' => 'zDv2V1bMTwk3my5bUAV3vkue1BJRTQ',
            'start_date' => $start_date,
            'end_date' => $end_date,
            'conversion_type' => 'all',
            'event_id' => 0,
            'affiliate_id' => '7746',
            'advertiser_id' => 0,
            'offer_id' => 0,
            'affiliate_tag_id' => 0,
            'advertiser_tag_id' => 0,
            'offer_tag_id' => 0,
            'campaign_id' => 0,
            'creative_id' => 0,
            'price_format_id' => 0,
            'disposition_type' => 'all',
            'disposition_id' => 0,
            'affiliate_billing_status' => 'all',
            'advertiser_billing_status' => 'all',
            'test_filter' => 'both',
            'start_at_row' => 0,
            'row_limit' => 0,
            'sort_field' => 'conversion_id',
            'sort_descending' => 'false',
        ];

        $params = http_build_query($params);
        $url = 'http://engageiq.performance-stats.com/api/11/reports.asmx/Conversions?'.$params;

        try {
            $data =  $this->client->request('get',$url);

            if ($data->getStatusCode() != 200) {
                $this->error("Can't fetch from Cake.");
                return false;
            }

            $data = $this->parseXmlResponse($data); // convert it into objecsts from xml

            collect($data->conversions)->filter(function ($conversions) {
                if (is_array($conversions)) {
                    return true;
                }

                $this->updateOfferStatus($conversions); // execute when its only 1 resulty

            })->flatten(1)->map(function ($conversion) {

                $this->updateOfferStatus($conversion); // execute when its have multiple result

            });

            $this->updateStatus(); // execute update status after loop above
        }
        catch(Exception $e)
        {
            return response(['error' => 'Server failure.', Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    protected function updateOfferStatus($conversion) // lets put the fetch data from cake to our own database for process
    {
        if (!is_object($conversion->sub_id_4) && $conversion->sub_id_4 >= 0)
        {
            UserConversion::firstOrCreate(
                [
                    'user_id' => $conversion->sub_id_4,
                    'creative_id' => $conversion->creative->creative_id,
                    'cake_id' => null,
                    'conversion_id' => $conversion->conversion_id,
                    'click_id' => $conversion->click_id,
                    'email' => !is_object($conversion->sub_id_5) ? $conversion->sub_id_5 : null
                ]
            );
        }
    }

    private function updateStatus()
    {
        $lists = UserConversion::where('executed',0)->get();
        \Log::notice('Cake Executed ->' .  Carbon::now());

        foreach ($lists as $list) {
            $user = User::find($list->user_id);

            if($user)
            {
                $hpcs = collect($user->hpcs_taken);
                $offers = collect($user->offers_taken);
                $freebies = collect($user->freebies_taken);

                if($matched = $hpcs->where('offer_id',$list->creative_id)->first())
                {
                    $newData = $this->preparedString($hpcs,$list->creative_id);
                    $user->hpcs_taken = $newData;
                    $list->executed = true;
                    $list->update();
                    $this->sendEmail($user->email, $matched->title, $user->first_name);
                }
                if ($matched = $offers->where('offer_id',$list->creative_id)->first())
                {
                    $newData = $this->preparedString($offers,$list->creative_id);
                    $user->offers_taken = $newData;
                    $user->update();
                    $list->executed = true;
                    $list->update();
                    $this->sendEmail($user->email, $matched->title, $user->first_name);
                }
                if ($matched = $freebies->where('offer_id',$list->creative_id)->first())
                {
                    $newData = $this->preparedString($freebies,$list->creative_id);
                    $user->freebies_taken = $newData;
                    $user->update();
                    $list->executed = true;
                    $list->update();
                    $this->sendEmail($user->email, $matched->title, $user->first_name);
                }
            }
        }
    }

    private function preparedString($collections,$creativeId)
    {
        $temporary = [];
        foreach ($collections as $collection)
        {
            if($collection['offer_id']==$creativeId)
            {
                $collection['status'] = 3;
                array_push($temporary,$collection);
            }
            else
            {
                array_push($temporary,$collection);
            }
        }
        return $temporary;
    }

    private function sendEmail($email='N/A', $title, $first_name){
        try{

            \Log::notice('Cake mail ->'. $email .  Carbon::now());
//                Mail::send('emails.cake', ['first_name' => $first_name, 'offer_name' => $title], function ($message) use ($email)
//                {
//                    $message->from('no-reply@highpayingcashsurveys.com', 'High Paying Cash Surveys');
//                    $message->to($email);
//                    $message->subject('High Paying Cash Survey - Send Payment');
//                });
            \Log::notice('Success mail'. $email .  Carbon::now());
        }
        catch(Exception $e)
        {
            \Log::notice('Cake Mail Error ->' . $email .Carbon::now());
        }

    }
}
