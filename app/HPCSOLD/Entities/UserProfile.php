<?php

namespace App\HPCSOLD\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $connection = 'mysql_2';

    public function user()
    {
        $this->belongsTo(User::class);
    }
}
