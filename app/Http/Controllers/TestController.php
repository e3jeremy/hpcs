<?php

namespace App\Http\Controllers;

use Aglipanci\Interspire\Facades\Interspire;
use App\Console\Commands\FetchConversionCake;
use App\HPCS\Entities\Campaign;
use App\HPCS\Entities\CoregLeads;
use App\HPCS\Entities\Designation;
use App\HPCS\Entities\Offer;
use App\HPCS\Entities\Opinions;
use App\HPCS\Entities\Question;

use App\HPCS\Entities\UserConversion;
use App\HPCS\LeadReactor\Mapper;
use App\HPCS\Entities\User as User;
use App\HPCS\Session\Guest;
use App\HPCSOLD\Entities\User as UserOld;
use App\HPCSPFR\Entities\User as UserPfr;

use App\Jobs\SendLeadsToLeadReactor;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

use App\HPCS\LeadReactor\Response as LeadReactorResponse;
use Jenssegers\Agent\Agent;
use Maatwebsite\Excel\Facades\Excel;

class TestController extends Controller
{


    protected $client;
    protected $offers;

    protected $user;


    public function __construct(UrlGenerator $url, Offer $offer)
    {
        $this->url = $url;
        $this->offers = $offer;
        $this->client = new Client();


    }

    public function index ($token)
    {
        $user = User::where('remember_token',$token)->first();

        if($user)
        {
            return view('frontend.password.reset')->withToken($token);

        }

        return 'Expire Link.. Error Code 403';

    }


    public function mapCampaign($id)
    {
        $campaign = Campaign::find($id);

        $user = User::find(347);

        $mapper = new Mapper($campaign, $user,'');

        $url = $this->nlr_url().'/sendLead/?' . $mapper->buildQuery();

        return $url;

    }

    public function reset(Requests\ForgotPasswordRequest $request){


        $user = User::where('email',$request->email)->first();

        if(!$user)
        {
            return 'Your email has not exist';
        }

        if(!$user->remember_token)
        {
            $user->remember_token = Str::random(60);
            $user->save();
        }

        $link = $this->url->to('/') . '/reset/password/confirmation/' . $user->remember_token;

        $mail = Mail::send('emails.send', ['title' => 'Reset Password', 'link' => $link ], function ($message) use ($user)
        {
            $message->from('no-reply@highpayingcashsurveys.com', 'High Paying Cash Surveys');
            $message->to($user->email);
            $message->subject('High Paying Cash Survey Reset Password');
        });

        return response(['message' => 'Success', 'code'=>200]);

    }

    public function nlr_url()
    {

//        return 'http://leadreactor.engageiq.com';

        if(config('app.env')=='production')
        {
            return 'http://leadreactor.engageiq.com';
        }

        return 'http://testleadreactor.engageiq.com';
    }



    public function update(Requests\UpdatePasswordRequest $request){

        $user = User::where('remember_token',$request->remember_token)->first();

        $user->password = $request->password;
        $user->remember_token = null;
        $user = $user->update();



        return response(['message' => 'Success', 'code'=>200]);
    }


    private function getNLRToken(){

        $client = new Client([
            'headers' => ['useremail'=>'api@engageiq.com','userpassword'=>'123456']
        ]);

        $response =  $client->post($this->nlr_url().'/getMyToken');
        $response =  json_decode(trim($response->getBody(),'(;)\\r\n'));

        return $response->token;
    }

    private function getNLRToken2(){

        $encryptionKey= config('hpcs.encryption_key');
        $macKey= config('hpcs.mac_key');
        $phpcrypt = new \PHPEncryptData\Simple($encryptionKey, $macKey);

        if(config('hpcs.encryption_applied'))
        {
            $username = $phpcrypt->encrypt(config('hpcs.nlr_username'));
            $password = $phpcrypt->encrypt(config('hpcs.nlr_password'));
        }
        else
        {
            $username = config('hpcs.nlr_username');
            $password = config('hpcs.nlr_password');
        }

        $client = new Client([
            'headers' => ['useremail'=>$username,'userpassword'=>$password]
        ]);

        $response =  $client->post($this->nlr_url().'/getMyToken');


        $response =  json_decode(trim($response->getBody(),'(;)\\r\n'));


        return $response->token;
    }

    private function getZipDetails($token,$zip)
    {
        if(!$token)
        {
            return false;
        }

        $client2 = new Client([
            'headers' => ['leadreactortoken'=>$token]
        ]);

        $response = $client2->get($this->nlr_url().'/zip_details?zip='.$zip);
        $response =  json_decode(trim($response->getBody(),'(;)\\r\n'));

        return $response;

    }

    public function clear()
    {
        $offers = Offer::select('id')->get();

        $collection = $offers->each(function ($item, $key) {

            $offer = Offer::find($item->id);

            $parts = parse_url($offer->link);
            parse_str($parts['query'], $query);

            if(isset($query['p']))
            {
                $offer->creative_id = 1;
            }

            $offer->link = null;
            $offer->update();
        });
    }

    public function sendpendinglist()
    {
        $pendings = CoregLeads::select('user_id')->where('status',1)->where('created_at','<',Carbon::now()->subMinute())->groupBy('user_id')->get();

        foreach ($pendings as $pending)
        {
            $user = User::find($pending->user_id);
            $this->dispatch((new SendLeadsToLeadReactor($user))->delay(1));
            \Log::notice('send pending leads execute of user ID = '.$pending->user_id);
        }
    }

    public function addSubscriberInterspire($email,$firstname,$lastname)
    {
        $url = config('interspire.url');
        $username = config('interspire.api_user');
        $usertoken = config('interspire.api_token');
        $list = config('interspire.list_target_id');

        $xml = "
             <xmlrequest>
                <username>{$username}</username>
                <usertoken>{$usertoken}</usertoken>
                <requesttype>subscribers</requesttype>
                <requestmethod>AddSubscriberToList</requestmethod>
                <details>
                    <emailaddress>{$email}</emailaddress>
                    <mailinglist>{$list}</mailinglist>
                    <format>html</format>
                    <confirmed>yes</confirmed>
                     <customfields>
                     
                             <item>
                                <fieldid>2</fieldid>
                                <value>{$firstname}</value>
                            </item>
                            <item>
                                <fieldid>3</fieldid>
                                <value>{$lastname}</value>
                            </item>

                 </customfields>
                </details>
            </xmlrequest>
        ";

        $ch = curl_init($url); //CHANGE TO THE PATH OF YOUR IEM INSTALLATION
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $result = @curl_exec($ch);
        if($result === false) {
//            die("Error performing request");
            \Log::notice('Interspire Connection Error -' . $email . '-'.  Carbon::now());
//            return 'Error Occured';
        }
        //var_dump($result); //for debugging purposes
        //Example of how to display returned data
        $xml_doc = simplexml_load_string($result);
        if ($xml_doc->status == 'SUCCESS' && empty($xml_doc->data))
        {
            \Log::notice('Interspire subscription success level 1 -' . $email . '-'.  Carbon::now());
        }
        if ($xml_doc->status == 'SUCCESS') {

            \Log::notice('Interspire subscription success level 2 -' . $email . '-'.  Carbon::now());

        } else {
           print_r($xml_doc->errormessage);
            \Log::notice('Error' . $email . '-' . $xml_doc->errormessage . '-'.  Carbon::now());
        }
    }


    public function test(Request $request)
    {

        Mail::send('emails.cake', ['first_name' => 'Hahaha Lol', 'offer_name' => 'wala wala'], function ($message)
        {
            $message->from('no-reply@highpayingcashsurveys.com', 'High Paying Cash Surveys');
            $message->to('engageiqwinston@gmail.com');
            $message->subject('High Paying Cash Survey - Send Payment');
        });

        dd('aw');

//        dd($this->addSubscriberInterspire('engageiqwinston@gmail.com',ucfirst('winston'),ucfirst('fale')));

//        $user = \Guest::user();
//        $offer = Offer::find(552);
//
//        $groups = Offer::select('id')->where('group_id',$offer->group_id)->where('group_id','<>',null)->get();
//        $penalties = 0;
//
//
//        if($offer->is_hpcs==1)
//        {
//            $tempo = $user->hpcs_taken;
//            $collection = collect($tempo);
//            if(!$tempo)
//            {
//                $tempo = [];
//            }
//
//            if(!$groups->isEmpty())
//            {
//                foreach($groups as $group)
//                {
//                    $pocket = $collection->where('offer_id',$group->id);
//                    if(!$pocket->isEmpty()) { $penalties++; }
//                }
//                $collection=null;
//            }
//            else
//            {
//                $collection = $collection->where('offer_id',$offer->id);
//
//                if(!$collection->isEmpty())
//                {
//                    $penalties++;
//                }
//            }
//
//            if($penalties<1)
//            {
//                array_push($tempo, ['is_Offer'=>1,'offer_id'=>$offer->id,'title'=>$offer->name . ' bonus', 'cash'=>$offer->hpcs_cash, 'status'=>1, 'taken' => Carbon::now()->format('m/d/Y')]);
//                $user->hpcs_taken = $tempo;
//            }
//
//        }


//        dd($this->addSubscriberInterspire('engageiqwinston@gmail.com'));

//        $timezone = 'America/Los_Angeles';
//        $now = Carbon::now()->tz($timezone)->format('m-d-Y');
//
//        dd($now);

//        dd($this->cake());

//       dd(Carbon::now()->addHours(5)->format('YmdHi'));

//        $campaigns = Campaign::select('name','id','sponsor')->get();
//
//        $campaigns = $campaigns->map(function ($campaign)
//        {
//            return $campaign->toArray();
//
//        })->toArray();

//        dd($campaigns);

//
//        $data = array(
//            array('data1', 'data2'),
//            array('data3', 'data4')
//        );
//
//
//        dd($data);


//
//        $user = User::find(4351);
//        $data = [];
//        foreach ($user->coregs as $coreg)
//        {
//            $result = collect(\GuzzleHttp\json_decode($coreg->pivot->nlr_response));
//            array_push($data,[
//                'campaign_name' => $coreg->campaign->name,
//                'status' => $coreg->pivot->status,
//                'reason' => $result['message']=='Lead received for processing' ? $result['realtime_meaning'] : $result['message'],
//                'nlr_visual' => $result['message']=='Lead received for processing' ? 'Yes' : 'No',
//            ]);
//        };
//
//        Excel::create($user->first_name.' '.$user->last_name.'- Coreg Report', function($excel) use ($data) {
//            // Set the title
//            $excel->setTitle('Our new awesome title');
//
//            // Chain the setters
//            $excel->setCreator('HPCS-REG')
//                ->setCompany('EIQ');
//            // Call them separately
//            $excel->setDescription('User Campaign Report');
//
//            $excel->sheet('Sheetname', function($sheet) use($data) {
//                $sheet->fromArray($data);
//            });
//        })->export('xlsx');


//        $user = \Guest::user();
//
//        dd(session()->get('prepop'));
//        $decimals = 2;
//        $min = 70;
//        $max = 100;
//
//        $scale = pow(10, $decimals);
//        dd(mt_rand($min * $scale, $max * $scale) / $scale);


        // not paid in hpcs
//        $user = User::where('payment_status', 'not like', '%"for":"hpcs"%')
//        ->orWhere('payment_status',null)
//        ->where('registered',1)
//        ->where('hpcs_taken','<>',null)
//        ->get();

        // not paid in challenge
//        $user = User::
//            where('payment_status', 'not like', '%"for":"challenge"%')
//            ->orWhere('payment_status',null)
//            ->where('created_at','<',Carbon::now()->subDay(30))
//            ->where('registered',1)
//            ->where('challenge_taken','<>',null)
//            ->get();
//
//        $user = User::
//        where('payment_status', 'like', '%"for":"challenge"%')
////            ->get();


//        $user = User::
//            where('registered',1)
//            ->where('hpcs_taken','like','%"status":3%')
//            ->get();

//        dd($user);

//        $device = [];
//        $agent = new Agent();
//        $device['type'] = $agent->isTablet() ? 'Tablet' : $agent->isMobile() ? 'Mobile' : $agent->isDesktop() ? 'Desktop' :  'Unknown';
//        $device['os'] = $agent->platform().' version '.$agent->version($agent->platform());
//        $device['agent'] = $_SERVER['HTTP_USER_AGENT'];
//        $device['browser'] = $agent->browser().' '.$agent->version($agent->browser());
//
//        dd($device);

//        return Carbon::now()->format('YmdHi');

//        $start_date = Carbon::today()->subDay(31)->format('m/d/Y');
//        $end_date = Carbon::today()->addDay(1)->format('m/d/Y');
//
//        $this->client = new Client();
//
//
//        $params = [
//            'api_key' => 'zDv2V1bMTwk3my5bUAV3vkue1BJRTQ',
//            'start_date' => $start_date,
//            'end_date' => $end_date,
//            'conversion_type' => 'all',
//            'event_id' => 0,
//            'affiliate_id' => '7746',
//            'advertiser_id' => 0,
//            'offer_id' => 0,
//            'affiliate_tag_id' => 0,
//            'advertiser_tag_id' => 0,
//            'offer_tag_id' => 0,
//            'campaign_id' => 0,
//            'creative_id' => 0,
//            'price_format_id' => 0,
//            'disposition_type' => 'all',
//            'disposition_id' => 0,
//            'affiliate_billing_status' => 'all',
//            'advertiser_billing_status' => 'all',
//            'test_filter' => 'both',
//            'start_at_row' => 0,
//            'row_limit' => 0,
//            'sort_field' => 'conversion_id',
//            'sort_descending' => 'false',
//        ];
//
//        $params = http_build_query($params);
//        $url = 'http://engageiq.performance-stats.com/api/11/reports.asmx/Conversions?'.$params;
//
//        try {
//            $data =  $this->client->request('get',$url);
//
//            if ($data->getStatusCode() != 200) {
//                $this->error("Can't fetch from Cake.");
//                return false;
//            }
//
//            $data = $this->parseXmlResponse($data);
//
//
//            dd($data->conversions);
//
//
//            collect($data->conversions)->filter(function ($conversions) {
//                if (is_array($conversions)) {
//                    return true;
//                }
//                $this->updateOfferStatus($conversions);
//
//            })->flatten(1)->map(function ($conversion) {
//                $this->updateOfferStatus($conversion);
//            });
//
//            $this->updateStatus();
//        }
//        catch(Exception $e)
//        {
//            return response(['error' => 'Server failure.', Response::HTTP_INTERNAL_SERVER_ERROR]);
//        }

//
//        $this->user = \Guest::user();
//
//
//        foreach($this->user->coregs->chunk(1) as $coreg)
//        {
//            $this->sendLead($coreg->first());
//        }
    }

    protected function updateOfferStatus($conversion) // lets put the fetch data from cake to our own database for process
    {
        if (!is_object($conversion->sub_id_4))
        {
            UserConversion::firstOrCreate(
                [
                    'user_id' => $conversion->sub_id_4,
                    'creative_id' => $conversion->creative->creative_id,
                    'cake_id' => null,
                    'conversion_id' => $conversion->conversion_id,
                    'click_id' => $conversion->click_id
                ]
            );
        }

    }


    private function updateStatus()
    {
        $lists = UserConversion::where('executed',0)->get();

        dd($lists);

        foreach ($lists as $list) {
            $user = User::find($list->user_id);

            if($user)
            {
                $hpcs = collect($user->hpcs_taken);
                $offers = collect($user->offers_taken);
                $freebies = collect($user->freebies_taken);

                if($hpcs->where('offer_id',$list->creative_id)->first())
                {
                    $newData = $this->preparedString($hpcs,$list->creative_id);
                    $user->hpcs_taken = $newData;
                    $list->executed = true;
                    $list->update();
                }
                if ($offers->where('offer_id',$list->creative_id)->first())
                {
                    $newData = $this->preparedString($offers,$list->creative_id);
                    $user->offers_taken = $newData;
                    $user->update();
                    $list->executed = true;
                    $list->update();
                }
                elseif ($freebies->where('offer_id',$list->creative_id)->first())
                {
                    $newData = $this->preparedString($freebies,$list->creative_id);
                    $user->freebies_taken = $newData;
                    $user->update();
                    $list->executed = true;
                    $list->update();
                }
            }


        }
    }


    protected function sendLead($lead)
    {
        //check if lead already sent
        if ($lead->pivot->status > 1) {
            return;
        }
        \Log::notice('Manual Send Leads -> user[' . $this->user->id . ']->question[' . $lead->pivot->question_id.']');

        try {
            $client = new Client();
            $mapper = new Mapper($lead->campaign, $this->user,$lead->pivot->additional_fields);

            $url = $this->nlr_url().'/sendLead/?' . $mapper->buildQuery();
            $lead->pivot->nlr_submitted = $url; //nlr submitted

            $response = $client->get($url);

            dd($response =  json_decode(trim($response->getBody(),'(;)\\r\n')));

            \Log::notice('send url inside ->' . $url);

            $this->updateStatus($response, $lead , $lead->campaign->freebie);
        } catch (Exception $e) {

            \Log::emergency('Leads Failed reason:' . $e->getMessage());
            $lead->pivot->save();
            return $e->getMessage();
        }
    }

    protected function acceptedCoreg($coreg)
    {
        $coreg->pivot->status = 3;
        $coreg->pivot->cash = $coreg->campaign->price;
        $coreg->pivot->save();
        echo "duplicate";

    }

    protected function declinedCoreg($coreg)
    {
        $coreg->pivot->status = 2;
        $coreg->pivot->cash = $coreg->campaign->price;
        $coreg->pivot->save();
        echo "success";
    }


    private function parseXmlResponse($response)
    {
        return json_decode(json_encode(simplexml_load_string($response->getBody()->getContents())));
    }


//    protected function updateOfferStatus($conversion)
//    {
//
//        if (!is_object($conversion->sub_id_4)) {
//            $cake_id = $this->offers->find($conversion->creative->creative_id);
//            $cake_id = $cake_id->cake_id;
//
//            $user_conversion = UserConversion::firstOrCreate(
//                [
//                    'user_id' => $conversion->sub_id_4,
//                    'creative_id' => $conversion->creative->creative_id,
//                    'cake_id' => $cake_id,
//                    'conversion_id' => $conversion->conversion_id,
//                    'click_id' => $conversion->click_id
//                ]
//            );
//
//        }
//
//    }

//    private function parseXmlResponse($response)
//    {
//        return json_decode(json_encode(simplexml_load_string($response->getBody()->getContents())));
//    }


//    private function updateStatus()
//    {
//        $lists =  $this->lists;
//
//        foreach ($lists as $list) {
//
//            $user = User::find($list->user_id);
//            $hpcs = collect($user->hpcs_taken);
//            $offers = collect($user->offers_taken);
//            $freebies = collect($user->freebies_taken);
//
//            if($hpcs->where('offer_id',$list->creative_id)->first())
//            {
//                $newData = $this->preparedString($hpcs,$list->creative_id);
//                $user->hpcs_taken = $newData;
//                $list->executed = true;
//                $list->update();
//            }
//
//            if ($offers->where('offer_id',$list->creative_id)->first())
//            {
//                $newData = $this->preparedString($offers,$list->creative_id);
//                $user->offers_taken = $newData;
//                $user->update();
//                $list->executed = true;
//                $list->update();
//            }
//            elseif ($freebies->where('offer_id',$list->creative_id)->first())
//            {
//                $newData = $this->preparedString($freebies,$list->creative_id);
//                $user->freebies_taken = $newData;
//                $user->update();
//                $list->executed = true;
//                $list->update();
//            }
//        }
//    }

    public function cake()
    {
        $start_date = Carbon::today()->subDay(9)->format('m/d/Y');
        $end_date = Carbon::today()->addDay(1)->format('m/d/Y');

        $params = [
            'api_key' => 'zDv2V1bMTwk3my5bUAV3vkue1BJRTQ',
            'start_date' => $start_date,
            'end_date' => $end_date,
            'conversion_type' => 'all',
            'event_id' => 0,
            'affiliate_id' => '7746',
            'advertiser_id' => 0,
            'offer_id' => 0,
            'affiliate_tag_id' => 0,
            'advertiser_tag_id' => 0,
            'offer_tag_id' => 0,
            'campaign_id' => 0,
            'creative_id' => 0,
            'price_format_id' => 0,
            'disposition_type' => 'all',
            'disposition_id' => 0,
            'affiliate_billing_status' => 'all',
            'advertiser_billing_status' => 'all',
            'test_filter' => 'both',
            'start_at_row' => 0,
            'row_limit' => 0,
            'sort_field' => 'conversion_id',
            'sort_descending' => 'false',
        ];

        $params = http_build_query($params);
        $url = 'http://engageiq.performance-stats.com/api/11/reports.asmx/Conversions?'.$params;

        try {
            $data =  $this->client->request('get',$url);

            if ($data->getStatusCode() != 200) {
                $this->error("Can't fetch from Cake.");
                return false;
            }

            $data = $this->parseXmlResponse($data); // convert it into objecsts from xml

            dd($data);

            collect($data->conversions)->filter(function ($conversions) {
                if (is_array($conversions)) {
                    return true;
                }

                $this->updateOfferStatus($conversions); // execute when its only 1 resulty

            })->flatten(1)->map(function ($conversion) {

                $this->updateOfferStatus($conversion); // execute when its have multiple result

            });


            $this->updateStatus(); // execute update status after loop above
        }
        catch(Exception $e)
        {
            return response(['error' => 'Server failure.', Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    private function preparedString($collections,$creativeId)
    {
        $temporary = [];
        foreach ($collections as $collection)
        {
            if($collection['offer_id']==$creativeId)
            {
                $collection['status'] = 3;
                array_push($temporary,$collection);
            }
            else
            {
                array_push($temporary,$collection);
            }
        }
        return $temporary;
    }


    public function getListInterspire()
    {
        $url = 'http://researchmailer.com/xml.php';

        $username = 'admin';
        $token = '28022a827209446dfa7ceaedd472dbd8b16eceac';

        $xml_data = '
            <xmlrequest>
                <username>' . $username . '</username>
                <usertoken>' . $token . '</usertoken>
                <requesttype>lists</requesttype>
                <requestmethod>GetLists</requestmethod>
                <details>
                    <start>0</start>
                    <perpage>50</perpage>
                </details>
            </xmlrequest>
        ';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_POST, true);
        if (!ini_get('safe_mode') && ini_get('open_basedir') == '') {
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        }
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        curl_setopt($ch, CURLOPT_POSTFIELDS, 'xml=' . $xml_data);

        $pageData = curl_exec($ch);

        if (!$pageData) {
            echo "Error!: " . curl_error($ch);
            echo "\n";
            exit;
        }
        curl_close($ch);

        return simplexml_load_string($pageData)->data;
    }

}
