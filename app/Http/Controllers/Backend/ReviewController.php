<?php

namespace App\Http\Controllers\Backend;

use App\HPCS\Entities\Review;
use App\Http\Requests\AddReview;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Symfony\Component\HttpFoundation\Response;

class ReviewController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => ['store']]);
    }

    public function store(AddReview $request)
    {
        Review::byCurrentUser($request->all())->save();
    }

    public function index()
    {
        return view('backend.review.review', compact('review'));
    }

    public function lists()
    {
         $review = Review::with('offer')->where('approved', 0)->orderBy('created_at','DESC')->paginate(15);
        
         return response(['data' => $review,'message'=>200]);
    }

    public function data()
    {
        $review = Review::select('id', 'author', 'comment', 'stars', 'created_at')
            ->where('approved', 0);

        return Datatables::of($review)
            ->addColumn('action', function ($review) {
                return '<a href="' . route('review.approve', $review->id) . '" class="btn blue btn-sm btn-outline sbold uppercase approve">
                     <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Approve
                  </a>' .
                '<a href="' . route('review.destroy', $review->id) . '" class="btn red btn-sm btn-outline sbold uppercase remove-btn reject"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i> Reject
                  </a>';
            })
            ->editColumn('created_at', function ($review) {
                return $review->created_at->diffForHumans();
            })
            ->editColumn('approved', function ($review) {
                return sprintf('<span class="label label-sm %s">%s</span>',
                    $review->approved ? 'label-success' : 'label-info',
                    $review->approved ? 'Approved' : 'Waiting for review');
            })
            ->make(true);
    }

    public function approve(Review $review)
    {
        $review->approved = 1;
        $review->save();

        \Cache::forget('offers');
        return $this->lists();
    }

    public function destroy($id)
    {
        Review::destroy($id);

        \Cache::forget('offers');
        return $this->lists();
    }
}
