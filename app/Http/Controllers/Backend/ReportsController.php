<?php

namespace App\Http\Controllers\Backend;

use App\HPCS\Entities\Campaign;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Cache;
use Mockery\CountValidator\Exception;

class ReportsController extends Controller
{


    protected $nlr_success;
    protected $nlr_reject;
    protected $adv_success;
    protected $adv_reject;

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->nlr_success = 0;
        $this->nlr_reject = 0;
        $this->adv_success = 0;
        $this->adv_reject = 0;
    }
    
    
    public function index()
    {
        return view('admin.reports');
    }

    public function data(Request $request)
    {

//        return Cache::remember('reports'.$request->date_from.'-'.$request->date_to, 10 , function () use ($request)
//        {

            $from = Carbon::parse($request->date_from);
            $to  = Carbon::parse($request->date_to);

            $campaigns = Campaign::with(['question' => function($q) use ($from,$to)
            {
                $q->with(['leads'=> function($q2) use ($from,$to) {
                    $q2->whereBetween('created_at', array($from, $to))->where('status','>',1)->where('nlr_response','<>',null);
                }]);
            }])->get();

            $campaigns = $campaigns->map(function($campaign)
            {
                $this->countLeads($campaign);

                $campaign['campaign'] = $campaign->question && $campaign->question->leads ? $campaign->question->leads->sortByDesc('created_at')->values()->all() : [];
                $campaign['nlr_success'] = $this->nlr_success;
                $campaign['nlr_rejected'] = $this->nlr_reject;
                $campaign['adv_success'] = $this->adv_success;
                $campaign['adv_rejected'] = $this->adv_reject;

                $campaign['active'] = $campaign->question && $campaign->question->status ? true: false;

                $campaign['total'] = $this->nlr_success+$this->nlr_reject;
                $campaign['conversions'] = $campaign['total']<1? 0 : round(($campaign['adv_success']/$campaign['total'])*100,2);

                return $campaign;
            });

            return response(['data' => $campaigns->sortByDesc($request->sorted_by)->values()->all(),'code'=>200]);

    }


    protected function countLeads($campaign)
    {
        $this->nlr_success = 0;
        $this->nlr_reject = 0;
        $this->adv_success = 0;
        $this->adv_reject = 0;

        try
        {
            if($campaign->question)
            {
                $campaign->question->leads->map(function($response){
                    $response_object = json_decode($response->nlr_response);

                    if($response_object->status=="lead_received")
                    {
                        $this->nlr_success++;

                        if(isset($response_object->realtime_meaning))
                        {
                            if($response_object->realtime_meaning=="Success")
                            {
                                $this->adv_success++;
                            }
                            else
                            {
                                $this->adv_reject++;
                            }
                        }
                    }
                    else
                    {
                        $this->nlr_reject++;
                    }


                });
            }

        }
        catch (\Exception $e)
        {
            dd($e);
        }


    }
    
    
    
    
    
}
