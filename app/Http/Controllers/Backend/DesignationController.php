<?php

namespace App\Http\Controllers\Backend;

use App\HPCS\Entities\Designation;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DesignationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function update(Request $request){

        $designation = Designation::find($request->id);
        $designation->header_big = $request->header_big;
        $designation->header_small = $request->header_small;
        $designation->no_of_questions = $request->no_of_questions;
        $designation->label = $request->label;
        $designation->price = $request->price;
        $designation->update();

        $designations = Designation::with('questions')->with('surveys')->orderBy('order','ASC')->get();
        return response(['data' => $designations, 'message'=>200]);
    }


}
