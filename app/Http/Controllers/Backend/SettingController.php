<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\HPCS\Entities\Setting;
use App\Http\Requests\UpdateSetting;

class SettingController extends Controller
{
    public function index()
    {
        $setting = Setting::first();
        return view('backend.setting', compact('setting'));
    }

    public function update(UpdateSetting $request)
    {
        $setting = Setting::first();
        $setting->fill($request->all())->save();
        flash('Successfully Updated', 'success');
        return back();
    }
}
