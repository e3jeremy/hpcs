<?php

namespace App\Http\Controllers\Backend;

use App\HPCS\Entities\Category;
use App\HPCS\Entities\CoregLeads;
use App\HPCS\Entities\Designation;
use App\HPCS\Entities\Offer;
use App\HPCS\Entities\Opinions;
use App\HPCS\Entities\Question;
use App\HPCS\Entities\UserConversion;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\HPCS\Entities\Review;
use App\HPCS\Entities\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {

        return view('admin.dashboard');
    }

    public function statistics()
    {
        return response(['hpcs' => $this->hpcsStatistics(),'message'=>200]);
    }


    public function hpcsStatistics()
    {
        $opinions = Opinions::where('created_at','>',Carbon::today())->count();
//        $user_counts = User::select('registered');

        $hpcs = User::select('registered','created_at')->get();

        $coregs =  CoregLeads::where('created_at','>',Carbon::today()->subDay(7))->whereIn('status',[2,3])->where('nlr_response','like','%"Lead received for processing"%')->get();

        $cake = UserConversion::where('created_at','>',Carbon::today()->subDay(7))->get();

        $hpcs_registered = $hpcs->where('registered',1);
        $hpcs_unregistered = $hpcs->where('registered',0);

        $data =
            [
                'overall' => 0,
                'registered' => 0,
                'unregistered' => 0,
                'opinions' => $opinions,
                'day8'=> [
                    'day_name' =>  Carbon::today()->subDay(7)->format('l'),
                    'registered' => $hpcs_registered->where('created_at','>=',Carbon::today()->subDay(7))->where('created_at','<',Carbon::today()->subDay(6))->count(),
                    'unregistered' =>$hpcs_unregistered->where('created_at','>=',Carbon::today()->subDay(7))->where('created_at','<',Carbon::today()->subDay(6))->count(),
                    'leads_success' =>   $coregs->where('created_at','>=',Carbon::today()->subDay(7))->where('created_at','<',Carbon::today()->subDay(6))->where('status',3)->count(),
                    'leads_rejected' =>   $coregs->where('created_at','>=',Carbon::today()->subDay(7))->where('created_at','<',Carbon::today()->subDay(6))->where('status',2)->count(),
                    'cake' =>   $cake->where('created_at','>=',Carbon::today()->subDay(7))->where('created_at','<',Carbon::today()->subDay(6))->count()
                ],
                'day7'=> [
                    'day_name' =>  Carbon::today()->subDay(6)->format('l'),
                    'registered' => $hpcs_registered->where('created_at','>=',Carbon::today()->subDay(6))->where('created_at','<',Carbon::today()->subDay(5))->count(),
                    'unregistered' => $hpcs_unregistered->where('created_at','>=',Carbon::today()->subDay(6))->where('created_at','<',Carbon::today()->subDay(5))->count(),
                    'leads_success' =>   $coregs->where('created_at','>=',Carbon::today()->subDay(6))->where('created_at','<',Carbon::today()->subDay(5))->where('status',3)->count(),
                    'leads_rejected' =>   $coregs->where('created_at','>=',Carbon::today()->subDay(6))->where('created_at','<',Carbon::today()->subDay(5))->where('status',2)->count(),
                    'cake' =>   $cake->where('created_at','>=',Carbon::today()->subDay(6))->where('created_at','<',Carbon::today()->subDay(5))->count()
                ],
                'day6'=> [
                    'day_name' =>  Carbon::today()->subDay(5)->format('l'),
                    'registered' => $hpcs_registered->where('created_at','>=',Carbon::today()->subDay(5))->where('created_at','<',Carbon::today()->subDay(4))->count(),
                    'unregistered' => $hpcs_unregistered->where('created_at','>=',Carbon::today()->subDay(5))->where('created_at','<',Carbon::today()->subDay(4))->count(),
                    'leads_success' =>   $coregs->where('created_at','>=',Carbon::today()->subDay(5))->where('created_at','<',Carbon::today()->subDay(4))->where('status',3)->count(),
                    'leads_rejected' =>   $coregs->where('created_at','>=',Carbon::today()->subDay(5))->where('created_at','<',Carbon::today()->subDay(4))->where('status',2)->count(),
                    'cake' =>   $cake->where('created_at','>=',Carbon::today()->subDay(5))->where('created_at','<',Carbon::today()->subDay(4))->count()
                ],
                'day5'=> [
                    'day_name' =>  Carbon::today()->subDay(4)->format('l'),
                    'registered' => $hpcs_registered->where('created_at','>=',Carbon::today()->subDay(4))->where('created_at','<',Carbon::today()->subDay(3))->count(),
                    'unregistered' => $hpcs_unregistered->where('created_at','>=',Carbon::today()->subDay(4))->where('created_at','<',Carbon::today()->subDay(3))->count(),
                    'leads_success' =>   $coregs->where('created_at','>=',Carbon::today()->subDay(4))->where('created_at','<',Carbon::today()->subDay(3))->where('status',3)->count(),
                    'leads_rejected' =>   $coregs->where('created_at','>=',Carbon::today()->subDay(4))->where('created_at','<',Carbon::today()->subDay(3))->where('status',2)->count(),
                    'cake' =>   $cake->where('created_at','>=',Carbon::today()->subDay(4))->where('created_at','<',Carbon::today()->subDay(3))->count()
                ],
                'day4'=> [
                    'day_name' =>  Carbon::today()->subDay(3)->format('l'),
                    'registered' => $hpcs_registered->where('created_at','>=',Carbon::today()->subDay(3))->where('created_at','<',Carbon::today()->subDay(2))->count(),
                    'unregistered' => $hpcs_unregistered->where('created_at','>=',Carbon::today()->subDay(3))->where('created_at','<',Carbon::today()->subDay(2))->count(),
                    'leads_success' =>   $coregs->where('created_at','>=',Carbon::today()->subDay(3))->where('created_at','<',Carbon::today()->subDay(2))->where('status',3)->count(),
                    'leads_rejected' =>   $coregs->where('created_at','>=',Carbon::today()->subDay(3))->where('created_at','<',Carbon::today()->subDay(2))->where('status',2)->count(),
                    'cake' =>   $cake->where('created_at','>=',Carbon::today()->subDay(3))->where('created_at','<',Carbon::today()->subDay(2))->count()
                ],
                'day3'=> [
                    'day_name' =>  Carbon::today()->subDay(2)->format('l'),
                    'registered' => $hpcs_registered->where('created_at','>=',Carbon::today()->subDay(2))->where('created_at','<',Carbon::yesterday())->count(),
                    'unregistered' => $hpcs_unregistered->where('created_at','>=',Carbon::today()->subDay(2))->where('created_at','<',Carbon::yesterday())->count(),
                    'leads_success' =>   $coregs->where('created_at','>=',Carbon::today()->subDay(2))->where('created_at','<',Carbon::yesterday())->where('status',3)->count(),
                    'leads_rejected' =>   $coregs->where('created_at','>=',Carbon::today()->subDay(2))->where('created_at','<',Carbon::yesterday())->where('status',2)->count(),
                    'cake' =>   $cake->where('created_at','>=',Carbon::today()->subDay(2))->where('created_at','<',Carbon::yesterday())->count()
                ],
                'day2'=> [
                    'day_name' =>  Carbon::yesterday()->format('l'),
                    'registered' => $hpcs_registered->where('created_at','>',Carbon::yesterday())->where('created_at','<',Carbon::today())->count(),
                    'unregistered' => $hpcs_unregistered->where('created_at','>',Carbon::yesterday())->where('created_at','<',Carbon::today())->count(),
                    'leads_success' =>  $coregs->where('created_at','>',Carbon::yesterday())->where('created_at','<',Carbon::today())->where('status',3)->count(),
                    'leads_rejected' =>  $coregs->where('created_at','>',Carbon::yesterday())->where('created_at','<',Carbon::today())->where('status',2)->count(),
                    'cake' =>  $cake->where('created_at','>',Carbon::yesterday())->where('created_at','<',Carbon::today())->count()
                ],
                'day1'=> [
                    'day_name' =>  'Today',
                    'registered' => $hpcs_registered->where('created_at','>=',Carbon::today())->count(),
                    'unregistered' => $hpcs_unregistered->where('created_at','>=',Carbon::today())->count(),
                    'leads_success' =>  $coregs->where('created_at','>=',Carbon::today())->where('status',3)->count(),
                    'leads_rejected' =>  $coregs->where('created_at','>=',Carbon::today())->where('status',2)->count(),
                    'cake' =>  $cake->where('created_at','>=',Carbon::today())->count()
                ]
            ];

        return $data;
    }




}
