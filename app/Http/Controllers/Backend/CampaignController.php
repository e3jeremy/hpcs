<?php

namespace App\Http\Controllers\Backend;

use App\HPCS\Entities\Campaign;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;

class CampaignController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => ['store']]);
    }


    public function getPagination(){

        $campaign = Campaign::where('published_in',0)->paginate(5);

        return response(['data' => $campaign,'message'=>200]);
    }

    public function destroy($id)
    {
        $campaign = Campaign::find($id);
        $campaign->destroy($id);

        $designations = Designation::with('questions')->with('surveys')->orderBy('order','ASC')->get();
        return response(['data' => $designations, 'message'=>200]);

    }

}
