<?php

namespace App\Http\Controllers\Frontend;

use App\HPCS\Entities\User;
use GuzzleHttp\Client;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

class CheckerController extends Controller
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function zip()
    {
        try {
            $response = $this->client->get('http://leadreactor.engageiq.com/zip_checker',['query' => ['zip' => request()->zip]]);
            if(trim($response->getBody()->getContents(),'(;)\\r\n"') == "true") {
                return response(['found' => true], Response::HTTP_ACCEPTED);
            }

            return response(['found' => false, Response::HTTP_NOT_FOUND]);

        }catch(Exception $e)
        {
            return response(['error' => 'Server failure.', Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    public function email()
    {
        if(! User::where('email', request()->email)->first())
        {
            return response(['found' => false, Response::HTTP_NOT_FOUND]);
        }

        return response(['found' => true], Response::HTTP_ACCEPTED);
    }

}
