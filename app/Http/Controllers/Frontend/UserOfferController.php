<?php

namespace App\Http\Controllers\Frontend;

use App\HPCS\Entities\CoregLeads;
use App\HPCS\Entities\Offer;
use App\HPCS\Repositories\Survey;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddUserOffer;
use Carbon\Carbon;
use Jenssegers\Agent\Agent;
use Symfony\Component\HttpFoundation\Response;

class UserOfferController extends Controller
{

    protected $survey;

    public function __construct(Survey $survey)
    {
        $this->survey = $survey;
    }


    public function store(AddUserOffer $request)
    {
        $user = \Guest::user();
        $offer = Offer::find($request->offer_id);


        $groups = Offer::select('id')->where('group_id',$offer->group_id)->where('group_id','<>',null)->get();
        $penalties = 0;


        if($offer->is_hpcs==1)
        {
            $tempo = $user->hpcs_taken;
            $collection = collect($tempo);
            if(!$tempo)
            {
                $tempo = [];
            }

            if(!$groups->isEmpty())
            {
                foreach($groups as $group)
                {
                    $pocket = $collection->where('offer_id',$group->id);
                    if(!$pocket->isEmpty()) { $penalties++; }
                }
                $collection=null;
            }
            else
            {
                $collection = $collection->where('offer_id',$offer->id);

                if(!$collection->isEmpty())
                {
                    $penalties++;
                }
            }

            if($penalties<1)
            {
                array_push($tempo, ['is_Offer'=>1,'offer_id'=>$offer->id,'title'=>$offer->name . ' bonus', 'cash'=>$offer->hpcs_cash, 'status'=>1, 'group'=>$offer->group_id, 'taken' => Carbon::now()->format('m/d/Y')]);
                $user->hpcs_taken = $tempo;
            }
        }

        else
        {
            if($offer->is_freebies==1)
            {
                $tempo = $user->freebies_taken;
                $collection = collect($tempo);
                if(!$tempo)
                {
                    $tempo = [];
                }

                $collection = $collection->where('offer_id',$offer->id);

                if($collection->isEmpty())
                {
                    array_push($tempo, ['is_Offer'=>1,'offer_id'=>$offer->id,'title'=>$offer->name, 'cash'=>$offer->cash, 'up_to'=>$offer->up_to, 'group'=>$offer->group_id, 'status'=>1, 'taken' => Carbon::now()->format('m/d/Y')]);
                    $user->freebies_taken = $tempo;
                }

            }
            if($offer->is_sponsors==1)
            {
                $tempo = $user->offers_taken;
                $collection = collect($tempo);
                if(!$tempo)
                {
                    $tempo = [];
                }

                $collection = $collection->where('offer_id',$offer->id);

                if($collection->isEmpty())
                {
                    array_push($tempo, ['is_Offer'=>1,'offer_id'=>$offer->id,'title'=>$offer->name, 'cash'=>$offer->cash, 'up_to'=>$offer->up_to, 'group'=>$offer->group_id, 'status'=>1, 'taken' => Carbon::now()->format('m/d/Y')]);
                    $user->offers_taken = $tempo;
                }
            }
        }

        $user->update();

        return response(['success' => 'true', 'user' =>  $this->survey->money($user) ], Response::HTTP_CREATED);
    }

    public function externalInner()
    {
        return view('frontend.home.external_inner_offer');
    }


    public function externalOuter()
    {
        if(!request()->referenceUrl)
        {
            abort(404);
        }

        $options = [
            'link' => request()->referenceUrl,
            'env' => config('app.env'),
            'setup' => 'offers'
        ];

        return view('frontend.home.external_outer_offer')
            ->withOptions(\GuzzleHttp\json_encode($options))
            ->withPrepop(json_encode($this->prepop()))->withDevice(json_encode($this->agent()));
    }

    public function facebookReward()
    {
        $user = \Guest::user();
        $tempo = $user->hpcs_taken;

        if(collect($user->hpcs_taken)->contains('title', 'Facebook Bonus'))
        {
            return $this->survey->money($user);
        }

        if(!$tempo) { $tempo = [];}
        array_push($tempo, ['is_Offer'=>0,'offer_id'=>null,'title'=>'Facebook Bonus', 'cash'=>1, 'status'=>4, 'taken' => Carbon::now()->format('m/d/Y')]);
        $user->hpcs_taken = $tempo;

        $user->update();

        return $this->survey->money(\Guest::user());
    }

    public function agent()
    {
        $device = [];

        $agent = new Agent();
        $device['type'] = $agent->isTablet() ? 'Tablet' : $agent->isMobile() ? 'Mobile' : $agent->isDesktop() ? 'Desktop' :  'Unknown';
        $device['os'] = $agent->platform().' version '.$agent->version($agent->platform());
        $device['agent'] = $_SERVER['HTTP_USER_AGENT'];
        $device['browser'] = $agent->browser().' '.$agent->version($agent->browser());

        return $device;
    }

    public function prepop()
    {
        $prepop = [];
        if($user = \Guest::user())
        {
            if(!$user->registered && request()->all())
            {
                $user['first_name'] = request()->firstname;
                $user['last_name'] = request()->lastname;
                $user['zip'] = request()->zip;
                $user['ip'] = request()->ip();
                $user['city'] = request()->city;
                $user['state'] = request()->state;
                $user['email'] = request()->email;
                $user['gender'] = request()->gender;
                $user['mail_address'] = request()->address;
                $user['phone1'] = request()->phone1;
                $user['phone2'] = request()->phone2;
                $user['phone3'] = request()->phone3;
                $user['dob'] = Carbon::createFromDate(request()->dobyear,request()->dobmonth,request()->dobday)->format('Y-m-d');

                $prepop = collect($user)->forget([
                    'coregs',
                    'designation_done',
                    'questions_answered',
                    'hpcs_taken',
                    'challenge_taken',
                    'offers_taken',
                    'freebies_taken',
                    'created_at',
                    'updated_at',
                    'first_challenge',
                    'last_challenge',
                    'offer_progress'
                ])->toArray();

                session(['prepop' => $prepop]);
            }
            else
            {
                session(['prepop' => null]);
            }
        }
        else
        {
            if(request()->all())
            {
                $prepop['first_name'] = request()->firstname;
                $prepop['last_name'] = request()->lastname;
                $prepop['zip'] = request()->zip;
                $prepop['ip'] = request()->ip();
                $prepop['city'] = request()->city;
                $prepop['state'] = request()->state;
                $prepop['email'] = request()->email;
                $prepop['gender'] = request()->gender;
                $prepop['mail_address'] = request()->address;
                $prepop['dob'] = Carbon::createFromDate(request()->dobyear,request()->dobmonth,request()->dobday)->format('Y-m-d');
                $prepop['phone1'] = request()->phone1;
                $prepop['phone2'] = request()->phone2;
                $prepop['phone3'] = request()->phone3;
                session(['prepop' => $prepop]);
            }
            else
            {
                session(['prepop' => null]);
            }
        }

        return $prepop;
    }
}
