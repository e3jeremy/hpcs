<?php

namespace App\Http\Controllers\Frontend;

use App\HPCS\Entities\Admin;
use App\Notifications\UserMadeContact;
use GuzzleHttp\Client;
use App\Http\Requests\SendContact;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function send(SendContact $request)
    {

        if(!$this->correctCaptcha($request))
        {
            return response(['success' => false], 200);
        }

        $admin = Admin::where('first_name','email')->first();
        $admin->notify(new UserMadeContact($request->all()));

        return response(['success' => true], 200);
    }

    /**
     * Google Recaptcha
     *
     * @param SendContact $request
     * @return boolean
     */
    private function correctCaptcha(SendContact $request)
    {
        $response = $this->client->post('https://www.google.com/recaptcha/api/siteverify', [
            'query' =>
                [
                    'response' => $request->input('g-recaptcha-response'),
                    'remoteip' => $request->ip(),
                    'secret' => config('services.recaptcha.secret_key')
                ]
        ]);
        $response = json_decode($response->getBody());

        return $response->success;
    }
}
