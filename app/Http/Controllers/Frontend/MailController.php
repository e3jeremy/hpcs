<?php

namespace App\Http\Controllers\Frontend;

use App\HPCS\Entities\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\SendContact;
use GuzzleHttp\Client;
use Illuminate\Contracts\Routing\UrlGenerator;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Pheanstalk\Exception;


class MailController extends Controller
{

    public function __construct(UrlGenerator $url,Client $client)
    {
        $this->url = $url;
        $this->client = $client;
    }

    public function index ($token)
    {
        $user = User::where('remember_token',$token)->first();

        if($user)
        {
            return view('frontend.password.reset')->withToken($token);
        }

        return 'Expire Link.. Error Code 403';
    }


    public function reset(Requests\ForgotPasswordRequest $request)
    {
        $user = User::where('email',$request->email)->first();

        if(!$user)
        {
            return response(['message' => 'Your email does not exist', 'code'=>404]);
        }

        if(!$user->remember_token)
        {
            $user->remember_token = Str::random(60);
            $user->save();
        }

        $link = $this->url->to('/') . '/reset/password/confirmation/' . $user->remember_token;

        Mail::send('emails.send', ['title' => 'Reset Password', 'link' => $link ], function ($message) use ($user)
        {
            $message->from('no-reply@highpayingcashsurveys.com', 'High Paying Cash Surveys');
            $message->to($user->email);
            $message->subject('High Paying Cash Survey Reset Password');
        });

        return response(['message' => 'Success', 'code'=>200]);
    }

    public function update(Requests\UpdatePasswordRequest $request){

        $user = User::where('remember_token',$request->remember_token)->first();

        $user->password = $request->password;
        $user->remember_token = null;
        $user->update();

        return response(['message' => 'Success', 'code'=>200]);
    }


    public function sendFeedback(SendContact $request)
    {
        if(!$this->correctCaptcha($request))
        {
            return response(['message' => 'captcha', 'code'=>403]);
        }

        $admin_email = config('hpcs.hpcs_admin_email');
        $ua=$this->getBrowser();
        $agent= $ua['name'] . " version " . $ua['version'] . " on " . $ua['userAgent'];

        $user = \Guest::user();

        if($user && $user->registered==1)
        {
            $request['email'] = $user->email;
            $request['name'] = $user->first_name .' '. $user->last_name;
            $request['zip'] = $user->zip;
            $request['city'] = $user->city;
            $request['state'] = $user->state;
        }

        $data = array(
            "email_address" =>$request->email,
            "topic" => $request->topic,
            "feedback" => $request->contents,
            "name" =>$request->name,
            "add_code" => '7746',
            "url" => $request->getHost(),
            "ip" => $request->ip(),
            'agent' => $agent,
            'zip' => $request->zip ?: '-',
            'city' => $request->city ?: '-',
            'state' => $request->state ?: '-',
            'page' => $request->page  ?: '-'
        );

        $emails = [
            'engageiqwinston@gmail.com',
            'francis@engageiq.com',
            'winston@engageiq.com',
            'ianrussel@engageiq.com',
            'dellengageiqcorp@engageiq.com',
            'shikha@engageiq.com',
//            'burt@engageiq.com',
            'ben@engageiq.com',
            'support@highpayingcashsurveys.com'
        ];

        try
        {
            Mail::send('emails.feedback',$data, function ($message) use ($request,$admin_email,$emails)
            {
                $message->from($request->email, 'High Paying Cash Surveys');
                $message->to($emails);
                $message->subject('HPCS-NEW Feedback');
            });
            return response(['message' => 'Success', 'code'=>200]);
        }
        catch(Exception $e)
        {
            return response(['message' => 'Error', 'code'=>500]);
        }
    }


    function getBrowser()
    {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        }
        elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        }
        elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }

        // Next get the name of the useragent yes seperately and for good reason
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
        {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        }
        elseif(preg_match('/Firefox/i',$u_agent))
        {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        }
        elseif(preg_match('/Chrome/i',$u_agent))
        {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        }
        elseif(preg_match('/Safari/i',$u_agent))
        {
            $bname = 'Apple Safari';
            $ub = "Safari";
        }
        elseif(preg_match('/Opera/i',$u_agent))
        {
            $bname = 'Opera';
            $ub = "Opera";
        }
        elseif(preg_match('/Netscape/i',$u_agent))
        {
            $bname = 'Netscape';
            $ub = "Netscape";
        }

        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }

        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                $version= $matches['version'][0];
            }
            else {
                $version= $matches['version'][1];
            }
        }
        else {
            $version= $matches['version'][0];
        }

        // check if we have a number
        if ($version==null || $version=="") {$version="?";}

        return array(
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'    => $pattern
        );
    }


    private function correctCaptcha(SendContact $request)
    {
        $response = $this->client->post('https://www.google.com/recaptcha/api/siteverify', [
            'query' =>
                [
                    'response' => $request->input('g-recaptcha-response'),
                    'remoteip' => $request->ip(),
                    'secret' => config('services.recaptcha.secret_key')
                ]
        ]);
        $response = json_decode($response->getBody());

        return $response->success;
    }
}
