<?php

namespace App\Http\Controllers\Auth;

use App\HPCS\Entities\CoregLeads;
use App\HPCS\Entities\User;
use App\HPCS\Repositories\Survey;
use App\Jobs\SendLeadsToLeadReactor;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'zip' => 'required|max:5',
            'dob' => 'required|date_format:"Y-m-d"',
            'gender' => 'in:m,f',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {

            flash()->overlay('Invalid data.', 'Please fill the registration form correctly.', 'warning');

            $this->throwValidationException(
                $request, $validator
            );
        }

        \Log::notice('Registration prompt for ->' . $request->email);

        $user = $this->saveUser($request);

        $this->guard()->login($user);

        flash()->overlay('You are now logged in.', 'Thank you.');

        $this->dispatch((new SendLeadsToLeadReactor($user))->delay(1));


        return redirect($this->redirectPath());
    }

    /**
     * Update user info
     * find user using uuid
     *
     * @param Request $request
     */
    protected function saveUser(Request $request)
    {
        $olduser = \Guest::user();
        if ($olduser == null || $olduser->registered) abort(403);

        $user = User::byUuid($olduser->uuid)->first();
        $user->fill($request->all());

        $zip_details = $this->getZipDetails($this->getNLRToken(),$request->zip);

        if($zip_details)
        {
            $user->state = $zip_details->state;
            $user->city = $zip_details->city;
        }
        else
        {
            $user->state = '';
            $user->city = '';
        }

        $user->registered = true;
        $user->ip = $request->ip();

        $user->save();

        //delete user cookie
        \Guest::deleteCookie();

        //update leads as submitted
        CoregLeads::where('user_id',$user->id)->where('status',0)->update(['status'=>1]);
        $survey = new Survey();

        //interspire notifcation
//        if(config('app.env')=='development' || config('app.env')=='local')
//        {
//            $this->addSubscriberInterspire($request->email,ucfirst($request->first_name),ucfirst($request->last_name));
//        }

        return $survey->money($user);
    }

    //custom functions

    private function getNLRToken(){

        if(config('hpcs.encryption_applied'))
        {
            $encryptionKey= config('hpcs.encryption_key');
            $macKey= config('hpcs.mac_key');
            $phpcrypt = new \PHPEncryptData\Simple($encryptionKey, $macKey);

            $username = $phpcrypt->encrypt(config('hpcs.nlr_username'));
            $password = $phpcrypt->encrypt(config('hpcs.nlr_password'));
        }
        else
        {
            $username = config('hpcs.nlr_username');
            $password = config('hpcs.nlr_password');
        }

        $client = new Client([
            'headers' => ['useremail'=>$username,'userpassword'=>$password]
        ]);

        $response =  $client->post($this->nlr_url().'/getMyToken');

        $response =  json_decode(trim($response->getBody(),'(;)\\r\n'));

        return $response->token;
    }

    private function getZipDetails($token,$zip)
    {
        if(!$token)
        {
            return false;
        }

        $client2 = new Client([
            'headers' => ['leadreactortoken'=>$token]
        ]);

        $response = $client2->get($this->nlr_url().'/zip_details?zip='.$zip);
        $response =  json_decode(trim($response->getBody(),'(;)\\r\n'));

        return $response;
    }

    public function nlr_url()
    {
        if(config('app.env')=='production')
        {
            return 'http://leadreactor.engageiq.com';
        }
        return 'http://testleadreactor.engageiq.com';
    }

    public function addSubscriberInterspire($email,$firstname,$lastname)
    {
        $url = config('interspire.url');
        $username = config('interspire.api_user');
        $usertoken = config('interspire.api_token');
        $list = config('interspire.list_target_id');

        $xml = "
             <xmlrequest>
                <username>{$username}</username>
                <usertoken>{$usertoken}</usertoken>
                <requesttype>subscribers</requesttype>
                <requestmethod>AddSubscriberToList</requestmethod>
                <details>
                    <emailaddress>{$email}</emailaddress>
                    <mailinglist>{$list}</mailinglist>
                    <format>html</format>
                    <confirmed>yes</confirmed>
                     <customfields>
                     
                             <item>
                                <fieldid>2</fieldid>
                                <value>{$firstname}</value>
                            </item>
                            <item>
                                <fieldid>3</fieldid>
                                <value>{$lastname}</value>
                            </item>

                 </customfields>
                </details>
            </xmlrequest>
        ";

        $ch = curl_init($url); //CHANGE TO THE PATH OF YOUR IEM INSTALLATION
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $result = @curl_exec($ch);
        if($result === false) {
            \Log::notice('Interspire Connection Error -' . $email . '-'.  Carbon::now());
        }
        $xml_doc = simplexml_load_string($result);
        if ($xml_doc->status == 'SUCCESS' && empty($xml_doc->data))
        {
            \Log::notice('Interspire subscription success level 1 -' . $email . '-'.  Carbon::now());
        }
        if ($xml_doc->status == 'SUCCESS') {
            \Log::notice('Interspire subscription success level 2 -' . $email . '-'.  Carbon::now());
        } else {
            \Log::notice('Error' . $email . '-' . $xml_doc->errormessage . '-'.  Carbon::now());
        }
    }

}
