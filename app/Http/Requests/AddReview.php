<?php

namespace App\Http\Requests;

class AddReview extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->guard('web')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'offer_id' => 'required',
            'comment' => 'required',
            'stars' => 'required'
        ];
    }
}
