<?php

namespace App\Providers;

use App\HPCS\Session\Guest;
use Illuminate\Support\ServiceProvider;

class GuestServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('guest', function () {
            return app(Guest::class);
        });
    }
}
