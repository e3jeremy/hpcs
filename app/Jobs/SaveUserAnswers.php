<?php

namespace App\Jobs;

use App\HPCS\Entities\CoregLeads;
use App\HPCS\Entities\Question;
use Carbon\Carbon;
use Mockery\CountValidator\Exception;

class SaveUserAnswers
{

    public $answers;

    public $designation;

    public $user;

    public function __construct($answers,$designation,$user,$additional,$questions_length)
    {
        $this->answers = $answers;
        $this->designation = $designation;
        $this->user = $user;
        $this->additional = $additional;
        $this->qlength  = $questions_length;
    }

    /**
     * Save User Answers to send later to lead
     * reactor if not registered
     *
     * @return void
     */
    public function saveUserAnswers()
    {
//        $answers = collect($this->answers);

        $userAnswers = collect($this->user->questions_answered);

        $answers = $this->answers->map(function ($item,$key) {
            return ['i'=>$key,'v'=>$item];
        });

        $filtered = $userAnswers->filter(function ($item, $key) use ($answers) {
            return !$answers->contains('i',$item['i']);
        });

        $this->user->questions_answered = collect($filtered)->merge($answers);
    }

    public function handle()
    {

        $this->saveUserAnswers();
        $questions = $this->getLeadQuestions();

        if(!$questions) {
            return true;
        }

        $user = $this->user;

        if($user->current_page_level == $user->max_page_level && $this->designation->price>0 && $user->can_challenge == 0)
        {
            $tempo = $user->hpcs_taken;

            if(!$tempo)
            {
                $tempo = [];
            }

            array_push($tempo, ['is_Offer'=>0,'offer_id'=>null,'title'=>$this->designation->label, 'cash'=>$this->designation->price, 'status'=>4, 'taken' => Carbon::now()->format('m/d/Y')]);

            $user->hpcs_taken = $tempo;
        }

        //challenge

        if($user->first_challenge && $user->current_page_level > 7)
        {
            if(Carbon::now()>$user->last_challenge) //FLAG HERE
            {
                $tempo = $user->challenge_taken;

                if(!$tempo)
                {
                    $tempo = [];
                }

                array_push($tempo, ['id'=>$user->challenge_designation, 'title'=>'Day '.$user->challenge_designation, 'cash'=>1, 'status'=>4, 'taken' => Carbon::now()->format('m/d/Y')]);
                $user->challenge_taken = $tempo;
            }
        }

        if($user->challenge_designation==30)
        {
            $user->can_challenge=2;
        }

        if($user->max_page_level==0 && $user->current_page_level==0)
        {
            $user->max_page_level = 1;
            $user->current_page_level = 1;
        }

        if($user->max_page_level>=$user->current_page_level)
        {
            if($user->can_challenge==1)
            {
                $user->max_page_level = 9;
                $user->current_page_level++;
            }
            else
            {
                if($user->max_page_level!=$user->current_page_level)
                {
                    $user->current_page_level++;
                }
                else
                {
                    $user->max_page_level++;
                    $user->current_page_level++;
                }

            }
        }

        $this->user->saveAnswers($questions);

        if($this->additional && $this->user->current_page_level > 3 && $this->user->current_page_level < 9)
        {
            foreach ($this->additional as $key => $value)
            {
                $leads = CoregLeads::where('user_id',$this->user->id)->where('question_id',$key)->get()->first();

                $leads->additional_fields=$value;
                $leads->update();
            }
        }

        $this->sendLeadsToLeadReactor();
        $this->user->earnings()->increment($this->designation);
    }

    /**
     * Dispatch send leads if the
     * user has registered
     */
    protected function sendLeadsToLeadReactor()
    {
        if ($this->user->registered || (session()->get('prepop') && $this->user->pp_tos) ) {
            $user = $this->user;
            CoregLeads::where('user_id',$user->id)->where('status',0)->update(['status'=>1]);
            dispatch((new SendLeadsToLeadReactor($this->user,session()->get('prepop')))->delay(1));
        }
    }
    /**
     * Return all lead reactor question where
     * the user clicked yes
     *
     * @return Question Collection
     */
    private function getLeadQuestions()
    {
        $questions = $this->getYeses();

        return $questions->filter(function ($question) {
            return $question->lead_reactor;
        });
    }

    /**
     * Return all questions where the user answers yes
     *
     * @return Question Collection
     */
    private function getYeses()
    {
        $yeses = $this->answers->filter(function ($value) {
            return $value > 0;
        });

        if($yeses->count()==$this->qlength && $this->user->current_page_level>2 && $this->user->current_page_level < 8)
        {
            $old = collect($this->user->designation_done);
            $this->user->designation_done = $old->push($this->user->current_page_level);
        }

        return $this->designation->questions->whereIn('id', $yeses->keys()->toArray());
    }


}
