<?php

namespace App\HPCS\LeadReactor;

use App\HPCS\Entities\Campaign;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;

class Mapper
{
    protected $user;

    protected $campaign;

    protected $query;

    protected $prepop;

    protected $equivalent = [
        'email' => 'email',
        'first_name' => 'first_name',
        'last_name' => 'last_name',
        'ip' => 'ip',
        'zip' => 'zip',
        'gender' => 'gender',
        'date_of_birth' => 'dob',
        'dob' => 'dob',
        'eiq_email' => 'email'
    ];

    public function __construct(Campaign $campaign, $user = [], $additional_fields, $prepop = [])
    {
        $this->prepop = $prepop;

        $this->user = $user;
        $this->campaign = $campaign; // solo campaigns 1 by 1
        $this->query = [];
        $this->paramenters = '';
        $this->additional_fields = $additional_fields;
    }

    public function buildQuery()
    {
        $this->buildConsumerFields();

        $this->buildIAdditionalFields();

        $this->buildIdentifierFields();

        return http_build_query($this->query,'', '&');
    }


    protected function buildIAdditionalFields()
    {
        $fields = json_decode($this->additional_fields);

        if($fields!=null)
        {
            foreach ($fields as $key => $field)
            {
                $this->query[key($field)] = $field->{key($field)};
            }
        }

    }
    protected function buildConsumerFields()
    {
        foreach($this->campaign->fields as $index => $field)
        {
            if(count($this->campaign->fields)>$index)
            {
                foreach ($field as $key => $value)
                {
                    if(substr($value, 0,7)=='static_')
                    {
                        $this->query[$key] = substr($value,7);
                    }
                    else
                    {
                        $this->query[$key] = $this->{$value}();

                    }
                }
            }
        }
    }

    protected function buildIdentifierFields()
    {
        $this->query['eiq_realtime'] = 1;
        $this->query['eiq_campaign_id'] = $this->campaign->eiq_campaign_id;
        $this->query['eiq_affiliate_id'] = config('hpcs.eiq_affiliate_id');
        $this->query['rev_tracker'] = config('hpcs.rev_tracker');

        $this->query['eiq_email'] = $this->email();

    }

    protected function programName()
    {
        return $this->campaign->name;
    }

    protected function leadid()
    {
        if($this->campaign->id==83)
        {
            return Carbon::now()->tz('America/Los_Angeles')->format('YmdHi');
        }
        return Carbon::now()->format('YmdHi');
    }

    protected function email()
    {
        return $this->user->email ? $this->user->email : $this->prepop['email'];
    }

    protected function fname()
    {
        return $this->user->first_name ? $this->user->first_name : $this->prepop['first_name'];
    }

    protected function lname()
    {
        return $this->user->last_name ? $this->user->last_name : $this->prepop['last_name'];
    }

    protected function ip()
    {
        return $this->user->ip ? $this->user->ip : $this->prepop['ip'];
    }

    protected function zip()
    {
        return $this->user->zip ? $this->user->zip : $this->prepop['zip'];
    }

    protected function birthdate1(){

        return $this->user->dob ? Carbon::parse($this->user->dob)->format('Y-m-d') : Carbon::parse($this->prepop['dob'])->format('Y-m-d');
    }

    protected function birthdate2(){
        return $this->user->dob ? Carbon::parse($this->user->dob)->format('m/d/Y') : Carbon::parse($this->prepop['dob'])->format('m/d/Y');
    }

    protected function dob(){
        return $this->user->dob ? Carbon::parse($this->user->dob)->format('Y-m-d') : Carbon::parse($this->prepop['dob'])->format('Y-m-d');
    }

    protected function dobday(){
        return $this->user->dob ? Carbon::parse($this->user->dob)->format('d') : Carbon::parse($this->prepop['dob'])->format('d');
    }

    protected function dobmonth(){
        return $this->user->dob ? Carbon::parse($this->user->dob)->format('m') : Carbon::parse($this->prepop['dob'])->format('m');
    }

    protected function dobyear(){
        return $this->user->dob ? Carbon::parse($this->user->dob)->format('y'): Carbon::parse($this->prepop['dob'])->format('y');
    }
    protected function title()
    {

        if($this->user->gender)
        {
            if($this->user->gender=='m' || $this->user->gender=='M')
            {
                return 'Mr.';
            }
            return 'Ms.';
        }
        else
        {
            if($this->prepop['gender']=='m' || $this->prepop['gender']=='M')
            {
                return 'Mr.';
            }
            return 'Ms.';

        }

    }

    protected function gy(){
        return "gy".config('hpcs.rev_tracker');
    }

    protected function gender(){

        if($this->user->gender)
        {
            if($this->user->gender=='m' || $this->user->gender=='M')
            {
                return 'M';
            }
            return 'F';
        }
        else
        {
            if($this->prepop['gender']=='m' || $this->prepop['gender']=='M')
            {
                return 'M';
            }
            return 'F';
        }


    }

    protected function genderFull()
    {
        if($this->user->gender)
        {
            if($this->user->gender=='m' || $this->user->gender=='M')
            {
                return 'Male';
            }
            return 'Female';
        }
        else
        {
            if($this->prepop['gender']=='m' || $this->prepop['gender']=='M')
            {
                return 'Male';
            }
            return 'Female';
        }
    }

    protected function createDate(){
        return Carbon::now()->format('m/d/Y H:i:s');
    }

    protected function timestamp1(){
        return Carbon::now()->format('m/d/Y H:i:s');
    }

    protected function datetime1(){
        return Carbon::now()->format('m/d/Y H:i:s');
    }

    protected function datetime2(){
        return Carbon::now()->format('m-d-Y H:i:s');
    }

    protected function datetime3(){

        if($this->campaign->id==83)
        {
            return Carbon::now()->tz('America/Los_Angeles')->format('m-d-Y H:i');
        }
        return Carbon::now()->format('m-d-Y H:i');
    }

    protected function city(){
        return $this->user->city ? $this->user->city : $this->prepop['city'];
    }

    protected function state(){
        return $this->user->state ? $this->user->state : $this->prepop['state'];
    }

    protected function address(){
        return $this->user->mail_address ? $this->user->mail_address : $this->prepop['mail_address'];
    }
    protected function phone(){
        return '0123456789';
    }

    protected function website(){
        return config('hpcs.rev_tracker');
    }

    protected function custom1(){

        return '1039_'.config('hpcs.rev_tracker');
    }

    protected function tpid(){

        return '123456';

    }

    protected function pvalue(){

        return config('hpcs.rev_tracker');

    }

    protected function source(){

        return "gy".config('hpcs.rev_tracker');
    }


    protected function addCodePadding(){
        return null;
    }


    protected function revTrackerMap(){
        return config('hpcs.rev_tracker');
    }


    protected function today(){
        return Carbon::now()->format('Y-m-d H:m:s');
    }

    protected function today2(){

        if($this->campaign->id==83)
        {
            return Carbon::now()->tz('America/Los_Angeles')->format('m-d-Y');
        }
        return Carbon::now()->format('Y-m-d');
    }

    protected function today3(){

        return Carbon::now()->format('m/d/Y');
    }

    protected function addCode(){
        return config('hpcs.rev_tracker');
    }

    protected function lr()
    {
        return 'new';
    }

    protected function age()
    {
        return $this->user->dob ? Carbon::parse($this->user->dob)->age : Carbon::parse($this->prepop['dob'])->age;
    }

    protected function postedstamp(){
        return Carbon::now()->format('Y-m-d H:m:s');
    }

    protected function dj(){
        return Carbon::now()->format('Y-m-d');
    }

    protected function debtAmount(){
        return 15000;
    }

    protected function url(){
        return App::make('url')->to('/');
    }
//    ------------------

    protected function date_of_birth()
    {
        return $this->user->dob ? Carbon::parse($this->user->dob)->format('m/d/Y') : Carbon::parse($this->prepop['dob'])->format('m/d/Y');
    }

    //eiq_campaign_id = 12 VindaleReasearch
//    protected function createDate()
//    {
//        return Carbon::now()->format('m/d/Y H:m:s');
//    }

    //eiq_campaign_id = 20 FusionCash
//    protected function postedstamp()
//    {
//        return Carbon::now()->format('m/d/Y H:m:s');
//    }

    //eiq_campaign_id = 30 Survey4Bucks
    protected function timestamp()
    {
        return Carbon::now()->format('m/d/Y H:m:s');
    }

    //eiq_campaign_id = 6 Mindspay
//    protected function today()
//    {
//        return Carbon::now()->format('m/d/Y H:m:s');
//    }

    //eiq_campaign_id = 14 Inboxpays
//    protected function dj()
//    {
//        return Carbon::now()->format('Y-m-d');
//    }

    //eiq_campaign_id = 6 Mindspay
    protected function rev_tracker_map()
    {
        return config('hpcs.rev_tracker');
    }
}