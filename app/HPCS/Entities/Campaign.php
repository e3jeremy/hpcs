<?php

namespace App\HPCS\Entities;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $fillable = [
        'deal',
        'eiq_campaign_id',
        'description',
        'freebie',
        'sponsor',
        'image',
        'fields',
        'dob_format',
        'freebie_image',
        'price',
        'freebie_f',
        'sponsor_f',
        'name',
        'additional_fields'
    ];

    protected $casts = [
        'fields' => 'array',
        'additional_fields' => 'array'
    ];

    public function question(){
        return $this->hasOne(Question::class,'id', 'published_in');
    }
}