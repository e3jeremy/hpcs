<?php

namespace App\HPCS\Entities;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use UuidKey;

    /**
     * The default values of the model.
     *
     * @var array
     */
    protected $attributes = [
        'hpcs_designation' => 1,
        'challenge_designation' => 1,
        'sponsors' => 0
    ];

    protected $fillable = [
        'first_name', 'last_name', 'zip',
        'gender', 'email', 'password',
        'dob', 'paypal', 'ip','city','state',
        'page_level','source_path',
        'mail_address'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $with = ['coregs.campaign'];

    protected $dates = ['last_challenge','first_challenge'];

    protected $casts = [
        'offers' => 'array',
        'challenge_taken' => 'array',
        'hpcs_taken' => 'array' ,
        'offers_taken' => 'array' ,
        'freebies_taken' => 'array',
        'questions_answered' => 'array',
        'designation_done' => 'array',
        'payment_status' => 'array'
    ];

    public static function scopeByUuid($query, $uuid)
    {
        return $query->where('uuid', $uuid);
    }

    public function coregs()
    {
        return $this->belongsToMany(Question::class,'coreg_leads')
            ->withPivot('status')
            ->withPivot('cash')
            ->withPivot('is_offers')
            ->withPivot('additional_fields')
            ->withPivot('nlr_response')
            ->withPivot('nlr_submitted')
            ->withTimestamps();
    }

    public function pending()
    {
        return $this->hasMany(CoregLeads::class);

    }

    public function offers()
    {
        return $this->belongsToMany(Offer::class)->withPivot('approved')->withTimestamps();
    }

    public function leads()
    {
        return $this->hasMany(CoregLeads::class);
    }

    public function freebies()
    {
        return $this->belongsToMany(Freebies::class)->withPivot('status')->withTimestamps();
    }
    public function cake()
    {
        return $this->hasMany(UserConversion::class);
    }

    public function earnings()
    {
        return new Earning($this);
    }

    public function saveAnswers($questions)
    {
        $this->coregs()->sync($questions,false);
    }

    public function attachOffer($offerId)
    {
        $this->offers()->sync([$offerId],false);

        $userOffers = $this->offers ?: [];
        $userOffers[] = $offerId;
        $this->offers = $userOffers;
        $this->save();
    }

    public function exceedDummyQuestions()
    {
        return $this->hpcs_designation > config('hpcs.max_hpcs');
    }

    public function allowChallenge()
    {
        $this->can_challenge = 1;
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }



}
