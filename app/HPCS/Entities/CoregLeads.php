<?php

namespace App\HPCS\Entities;

use Illuminate\Database\Eloquent\Model;

class CoregLeads extends Model
{
    protected $casts = [
        'additional_fields' => 'array'
    ];

    public function users(){
        $this->hasOne(User::class);
    }

    public function campaign(){
        $this->belongsTo(Campaign::class);
    }

    public function questions(){
        $this->belongsTo(Question::class,'id','question_id');
    }
}
