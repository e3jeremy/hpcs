<?php

namespace App\HPCS\Entities;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $fillable = ['label'];

    //not yet used
}