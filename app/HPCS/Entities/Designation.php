<?php

namespace App\HPCS\Entities;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $fillable = [
        'label',
        'description',
        'header_big',
        'header_big_class',
        'header_small',
        'header_small_class',
        'content',
        'content_class',
        'no_of_questions',
        'challenge',
        'price',
        'survey_id',
        'order'
    ];

    public function questions()
    {
        return $this->hasMany(Question::class,'designation_id',$this->order);
    }

    private function questionnaires()
    {
       $this->questions()->get();
    }

    public function surveys()
    {
        return $this->hasOne(Survey::class,'id','survey_id');
    }
}