<?php

namespace App\HPCS\Entities;

use Illuminate\Database\Eloquent\Model;

class Freebies extends Model
{
    protected $fillable = [
        'name',
        'subtitle',
        'description',
        'image',
        'cash',
        'our_rating'
    ];
}
