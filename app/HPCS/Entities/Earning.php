<?php

namespace App\HPCS\Entities;

use Carbon\Carbon;

class Earning
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function increment($designation)
    {
        if ($designation->challenge) {
            $this->incrementChallenge($designation->price);
            $this->incrementChallengeDesignation();
            $this->user->save();
        }

        // HPCS SURVEY
        if ($this->user->hpcs_designation > Setting::maxHpcs()) {
            return;
        }
        $this->incrementHpcs($designation->price);

        $this->incrementHPCSDesignation();

        //if the users designation has triggered challege
        if ($this->user->hpcs_designation > Setting::maxHpcs()) {
            $this->user->allowChallenge();
        }

        $this->user->save();
    }

    public function incrementSponsors($price)
    {
        $this->user->sponsors += $price;
        $this->user->save();
    }

    protected function incrementChallenge($price)
    {
        $this->user->challenge += $price;
    }

    protected function incrementHpcs($price)
    {
        $this->user->hpcs += $price;
    }

    protected function incrementHPCSDesignation()
    {
        $this->user->hpcs_designation += 1;
    }

    protected function incrementChallengeDesignation()
    {
        $this->user->challenge_designation += 1;
        $this->user->challenge_notification = 0;
        $this->user->last_challenge = Carbon::now();
    }
}