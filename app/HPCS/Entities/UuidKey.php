<?php

namespace App\HPCS\Entities;

use Ramsey\Uuid\Uuid;

trait UuidKey
{
    /**
     * Boot the Uuid trait for the model.
     *
     * @return void
     */
    public static function bootUuidKey()
    {
        static::creating(function ($model) {
            $model->uuid = (string)Uuid::uuid4();
        });
    }
}