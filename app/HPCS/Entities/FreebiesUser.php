<?php

namespace App\HPCS\Entities;

use Illuminate\Database\Eloquent\Model;

class FreebiesUser extends Model
{
    protected  $table = 'freebies_user';

    protected $fillable = [
        'user_id',
        'freebies_id',
        'cash',
        'status',
    ];
}
