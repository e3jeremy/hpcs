<?php

// Authentication Routes...
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');
Route::get('logout', 'Auth\LoginController@logout');

// Registration Routes...
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


Route::group(['namespace' => 'Backend', 'prefix' => 'admin'], function () {
    Route::get('setting', 'SettingController@index');
    Route::post('setting', 'SettingController@update');
});



//Route to new layout

Route::get('new-layout', function () {
    return view('frontend.new-layout');
});

// route to modal-sponsors
Route::get('modal-sponsors', function () {
    return view('frontend.modal-sponsors');
});

Route::group(['prefix' => 'admin'], function(){
    
    // Add question
    Route::get('question/create', function () {
        return view('backend.question.add');
    });

    // Add co-reg question
    Route::get('question/coreg/create', function () {
        return view('backend.question.add-coreg');
    });



    // Add user
    // Route::get('user/create', function () {
    //     return view('backend.user.add');
    // });

    // User list
    // Route::get('user', function () {
    //     return view('backend.user.list');
    // });

    // Add zipcode
    Route::get('zipcode/create', function () {
        return view('backend.zipcode.add');
    });

    // Zipcode list
    Route::get('zipcode', function () {
        return view('backend.zipcode.list');
    });

    // Add Good comment
//    Route::get('comment/create', function () {
//        return view('backend.good-comments.add');
//    });

    // Good Comment list
    Route::get('comment', function () {
        return view('backend.good-comments.list');
    });

    // Add feedback
    Route::get('feedback/create', function () {
        return view('backend.feedback.add');
    });

    // feedback list
    Route::get('feedback', function () {
        return view('backend.feedback.list');
    });

    // Bad Comment list
    Route::get('comment/bad', function () {
        return view('backend.bad-comments.list');
    });

    // Bad Comment filter
    Route::get('comment/filter', function () {
        return view('backend.bad-comments.filter');
    });

    // User Profile
    Route::get('profile', function () {
        return view('backend.profile.profile');
    });
});




Route::get('/test/test','TestController@test');

//Reverse for back and forward buttons
Route::get('/user/adjust', [
    'as' => 'user.adjust',
    'uses' => 'AdjustController@adjust'
]);


// Route to new dashboard by Ben's mocks
Route::get('new-dashboard', function () {
    return view('ben-admin.dashboard');
});

Route::get('user', function () {
    return view('ben-admin.user');
});

Route::post('opinion', [
    'as' => 'opinion.send',
    'uses' => 'OpinionController@store'
]);


Route::get('campaign/map/test/{id}', [
    'as' => 'map.campaign.test',
    'uses' => 'TestController@mapCampaign'
]);



