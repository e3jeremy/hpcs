var confirm = function (url, callback, before) {
    var before = before || function () {};
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, remove it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }, function () {
        before();
        $.ajax({
            url:url,
            method:'DELETE',
            data: { _token: $('[name="_token"]').attr('content')}
        }).success(function(data) {
            notify("Deleted!", "", "success");
            callback(data);
        }).fail(function(e) {
            notify(e.statusText || ($.parseJSON(e.responseText)).message, "", "error", 'overlay');
        });

    });
};

var confirmFunnel = function (url, property, goal, callback) {
    var before = before || function () {};
    swal({
        title: "Are you sure to remove this funnel?",
        text: "You will not be able to recover this.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, remove it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }, function () {
        before();
        $.ajax({
            url:url,
            method:'DELETE',
            data: {_token: $('[name="_token"]').attr('content'), property : property, goal : goal }
        }).success(function(data) {
            notify("Deleted!", "", "success");
            callback(data);
        }).fail(function(e) {
            notify(e.statusText || ($.parseJSON(e.responseText)).message, "", "error", 'overlay');
        });

    });
};


var confirmUpdate = function (url, text, callback) {
    swal({
        title: "Are you sure?",
        text: text,
        type: "info",
        showCancelButton: true,
        confirmButtonText: "Yes, do it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }, function () {
        $.ajax({
            url:url,
            method:'PATCH',
            data: { _token: $('[name="_token"]').attr('content') }
        }).success(function(data) {
            notify("Success!", "", "success");
            callback(data.id);
        }).fail(function(e) {
            notify(e.statusText || ($.parseJSON(e.responseText)).message, "", "error", 'overlay');
        });

    });
};


var confirmUpdateQuestions = function (url, text, data, callback) {
    swal({
        title: "Are you sure?",
        text: text,
        type: "info",
        showCancelButton: true,
        confirmButtonText: "Yes, do it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }, function () {
        $.ajax({
            url:url,
            method:'PATCH',
            data: data
        }).success(function(data) {
            notify("Success!", "", "success");
            callback(data);
        }).fail(function(e) {
            notify(e.statusText || ($.parseJSON(e.responseText)).message, "", "error", 'overlay');
        });
    });
};

var notify = function (title, message, level, type) {

    level = level || 'success';

    if (type == 'overlay') {
        swal({
            title: title,
            text: message,
            type: level,
            confirmButtonText: "Okay"
        });
    } else if (type == 'progress') {
        swal({
            title: title,
            text: message,
            type: level,
            showConfirmButton: false
        });
    } else {
        swal({
            title: title,
            text: message,
            type: level,
            showConfirmButton: false,
            timer: 1700
        });
    }
};


