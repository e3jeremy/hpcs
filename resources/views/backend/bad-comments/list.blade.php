@extends('backend.layouts.main')
@section('content')
@section('title','Bad Comments List')
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>Bad Comments List</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">Bad Comments List</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN MAIN CONTENT -->
<div class="row">
	<div class="col-md-12">
	<div class="portlet box blue">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-exclamation-triangle"></i>
        <span class="caption-subject bold uppercase"> Bad Comments List</span>
      </div>
    </div>
    <div class="portlet-body">
    	<table class="table table-hover">
    		<thead>
    			<tr>
    				<th>Sr.#</th>
    				<th>User</th>
    				<th>Email</th>
    				<th>Survey</th>
    				<th>Status</th>
                <th>Date Created</th>
    				<th>Actions</th>
    			</tr>
    		</thead>
    		<tbody>
    			@for($i = 1; $i<=10; $i++)
    				<tr>
    					<td>{{ $i }}</td>
                    <td>Muz</td>
                    <td>muzsports@gmail.com</td>
    					<td><img src="/images/images_survey/cpawall_freebiesfrenzy.png" class="img-responsive" alt=""></td>
                    <td><span class="label label-sm label-info"> Waiting for review </span></td>
                    <td>1 week ago</td>
                    <td></td>
    				</tr>
    			@endfor
    		</tbody>
    	</table>
    	<div class="text-right">
			  <ul class="pagination pagination-sm">
			    <li>
			      <a href="javascript:;">
			      <i class="fa fa-angle-left"></i>
			      </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 1 </a>
			    </li>
			    <li class="active">
			      <a href="javascript:;"> 2 </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 3 </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 4 </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 5 </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 6 </a>
			    </li>
			    <li>
			      <a href="javascript:;">
			      <i class="fa fa-angle-right"></i>
			      </a>
			    </li>
			  </ul>
			</div>
    </div>
   </div>
	</div>
</div>
<!-- END MAIN CONTENT -->
@stop