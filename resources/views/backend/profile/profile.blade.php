@extends('backend.layouts.main')
@section('content')
@section('title', 'My Profile')
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>My Profile</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">My Profile</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN MAIN CONTENT -->
<div class="row">
	<div class="col-md-12">
		<div class="profile-sidebar">
	  <!-- PORTLET MAIN -->
	  <div class="portlet light profile-sidebar-portlet bordered">
	    <!-- SIDEBAR USERPIC -->
	    <div class="profile-userpic">
	      <img src="/images/profile.jpg" class="img-responsive" alt=""> 
	    </div>
	    <!-- END SIDEBAR USERPIC -->
	    <!-- SIDEBAR USER TITLE -->
	    <div class="profile-usertitle">
	      <div class="profile-usertitle-name"> sree	kanth </div>
	      <div class="profile-usertitle-job"> admin@info.com </div>
	    </div>
	    <!-- END SIDEBAR USER TITLE -->
	    <!-- SIDEBAR BUTTONS -->
	    <!-- END SIDEBAR BUTTONS -->
	    <!-- SIDEBAR MENU -->
	    <div class="profile-usermenu">
	    </div>
	    <!-- END MENU -->
	  </div>
	  <!-- END PORTLET MAIN -->
	  <!-- PORTLET MAIN -->
	  <div class="portlet light bordered">
	    <!-- STAT -->
	    <div class="row list-separated profile-stat">
	      <div class="col-md-6 col-sm-6 col-xs-6">
	        <div class="uppercase profile-stat-title"> 1 </div>
	        <div class="uppercase profile-stat-text"> ID </div>
	      </div>
	      <div class="col-md-6 col-sm-6 col-xs-6">
	        <div class="uppercase profile-stat-title"> 51 </div>
	        <div class="uppercase profile-stat-text"> Money </div>
	      </div>
	    </div>
	    <!-- END STAT -->
	    <div>
	      <h4 class="profile-desc-title">About sree	kanth</h4>
	      <span class="profile-desc-text"> Lorem ipsum dolor sit amet diam nonummy nibh dolore. </span>
	      <div class="margin-top-20 profile-desc-link">
	        <i class="fa fa-globe"></i>
	        <a href="#">sreekanth.com</a>
	      </div>
	      <div class="margin-top-20 profile-desc-link">
	        <i class="fa fa-twitter"></i>
	        <a href="#">@sree</a>
	      </div>
	      <div class="margin-top-20 profile-desc-link">
	        <i class="fa fa-facebook"></i>
	        <a href="#">sree</a>
	      </div>
	    </div>
	  </div>
	  <!-- END PORTLET MAIN -->
	</div>
  <div class="profile-content">
  	<div class="col-md-12">
  		<div class="portlet box blue">
       <div class="portlet-title tabbable-line">
           <div class="caption caption-md">
               <i class="icon-globe theme-font hide"></i>
               <span class="caption-subject bold uppercase">Profile Account</span>
           </div>
       </div>
       <div class="portlet-body">
       	<div class="tabbable-custom">
       		<ul class="nav nav-tabs">
               <li class="active">
                   <a href="#tab_1_1" data-toggle="tab" aria-expanded="true">Personal Info</a>
               </li>
               <li class="">
                   <a href="#tab_1_2" data-toggle="tab" aria-expanded="false">Change Avatar</a>
               </li>
               <li class="">
                   <a href="#tab_1_3" data-toggle="tab" aria-expanded="false">Change Password</a>
               </li>
               <li>
                   <a href="#tab_1_4" data-toggle="tab">Privacy Settings</a>
               </li>
           </ul>
           <div class="tab-content">
               <!-- PERSONAL INFO TAB -->
               <div class="tab-pane active" id="tab_1_1">
                   <form role="form" action="#">
                   			<div class="form-group">
                         <label class="control-label">Date Created</label>
                         <input type="text" placeholder="2014-02-21 12:53:06" class="form-control input-lg" readonly>
                       </div>
                       <div class="form-group">
                         <label class="control-label">First Name</label>
                         <input type="text" placeholder="Sree" class="form-control input-lg">
                       </div>
                       <div class="form-group">
                         <label class="control-label">Last Name</label>
                         <input type="text" placeholder="Kanth" class="form-control input-lg">
                       </div>
                       <div class="form-group">
                         <label class="control-label">Email</label>
                         <input type="email" placeholder="admin@info.com" class="form-control input-lg">
                       </div>
                       <div class="form-group">
                         <label class="control-label">Mobile Number</label>
                         <input type="text" placeholder="+1 646 580 DEMO (6284)" class="form-control input-lg">
                       </div>
                       <div class="form-group">
                         <label class="control-label">Birth Date</label>
                         <input type="text" placeholder="12-21-1978" class="form-control input-lg">
                       </div>
                       <div class="form-group">
                           <label class="control-label">Gender</label>
                            <select class="form-control input-lg">
                            	<option>Male</option>
                            	<option>Female</option>
                            </select>
                       </div>
                       <div class="form-group">
                           <label class="control-label">Address</label>
                           <input type="text" placeholder="3080 olcott street" class="form-control input-lg"> </div>
                       <div class="form-group">
                         <label class="control-label">City</label>
                         <input type="text" placeholder="Santa Clara" class="form-control input-lg">
                       </div>
                       <div class="form-group">
                         <label class="control-label">State</label>
                         <input type="text" placeholder="California" class="form-control input-lg">
                       </div>
														<div class="form-group">
                         <label class="control-label">Country</label>
                         <input type="text" placeholder="USA" class="form-control input-lg">
                       </div>
                       <div class="form-group">
                           <label class="control-label">About</label>
                           <textarea class="form-control input-lg description" rows="3" placeholder="Lorem ipsum dolor sit amet diam nonummy nibh dolore."></textarea>
                       </div>
                       <div class="form-group">
                         <label class="control-label">Twitter</label>
                         <input type="text" placeholder="@sree" class="form-control input-lg">
                       </div>
                       <div class="form-group">
                         <label class="control-label">Facebook</label>
                         <input type="text" placeholder="sree" class="form-control input-lg">
                       </div>
                       <div class="margiv-top-10">
                           <a href="javascript:;" class="btn btn-lg blue"><i class="fa fa-save"></i> Save Changes </a>
                           <a href="javascript:;" class="btn btn-lg default"> Cancel </a>
                       </div>
                   </form>
               </div>
               <!-- END PERSONAL INFO TAB -->
               <!-- CHANGE AVATAR TAB -->
               <div class="tab-pane" id="tab_1_2">
                   <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                       laborum eiusmod. </p>
                   <form action="#" role="form">
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;"></div>
                          <div>
                              <span class="btn blue btn-lg btn-file">
                                  <span class="fileinput-new"> Select image </span>
                                  <span class="fileinput-exists"> Change </span>
                                  <input type="hidden" value="" name="..."><input type="file" name=""> </span>
                              <a href="javascript:;" class="btn btn-lg red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                          </div>
                      </div>
                   </form>
               </div>
               <!-- END CHANGE AVATAR TAB -->
               <!-- CHANGE PASSWORD TAB -->
               <div class="tab-pane" id="tab_1_3">
                   <form action="#">
                       <div class="form-group">
                           <label class="control-label">Current Password</label>
                           <input type="password" class="form-control input-lg"> </div>
                       <div class="form-group">
                           <label class="control-label">New Password</label>
                           <input type="password" class="form-control input-lg"> </div>
                       <div class="form-group">
                           <label class="control-label">Re-type New Password</label>
                           <input type="password" class="form-control input-lg"> </div>
                       <div class="margin-top-10">
                           <a href="javascript:;" class="btn btn-lg blue"> Change Password </a>
                           <a href="javascript:;" class="btn btn-lg default"> Cancel </a>
                       </div>
                   </form>
               </div>
               <!-- END CHANGE PASSWORD TAB -->
               <!-- PRIVACY SETTINGS TAB -->
               <div class="tab-pane" id="tab_1_4">
                   <form action="#">
                       <table class="table table-light table-hover">
                           <tbody><tr>
                               <td> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus.. </td>
                               <td>
                                   <div class="mt-radio-inline">
                                       <label class="mt-radio">
                                           <input type="radio" name="optionsRadios1" value="option1"> Yes
                                           <span></span>
                                       </label>
                                       <label class="mt-radio">
                                           <input type="radio" name="optionsRadios1" value="option2" checked=""> No
                                           <span></span>
                                       </label>
                                   </div>
                               </td>
                           </tr>
                           <tr>
                               <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                               <td>
                                   <div class="mt-radio-inline">
                                       <label class="mt-radio">
                                           <input type="radio" name="optionsRadios11" value="option1"> Yes
                                           <span></span>
                                       </label>
                                       <label class="mt-radio">
                                           <input type="radio" name="optionsRadios11" value="option2" checked=""> No
                                           <span></span>
                                       </label>
                                   </div>
                               </td>
                           </tr>
                           <tr>
                               <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                               <td>
                                   <div class="mt-radio-inline">
                                       <label class="mt-radio">
                                           <input type="radio" name="optionsRadios21" value="option1"> Yes
                                           <span></span>
                                       </label>
                                       <label class="mt-radio">
                                           <input type="radio" name="optionsRadios21" value="option2" checked=""> No
                                           <span></span>
                                       </label>
                                   </div>
                               </td>
                           </tr>
                           <tr>
                               <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                               <td>
                                   <div class="mt-radio-inline">
                                       <label class="mt-radio">
                                           <input type="radio" name="optionsRadios31" value="option1"> Yes
                                           <span></span>
                                       </label>
                                       <label class="mt-radio">
                                           <input type="radio" name="optionsRadios31" value="option2" checked=""> No
                                           <span></span>
                                       </label>
                                   </div>
                               </td>
                           </tr>
                       </tbody></table>
                       <!--end profile-settings-->
                       <div class="margin-top-10">
                           <a href="javascript:;" class="btn btn-lg blue"> Save Changes </a>
                           <a href="javascript:;" class="btn btn-lg default"> Cancel </a>
                       </div>
                   </form>
               </div>
               <!-- END PRIVACY SETTINGS TAB -->
           </div>
         </div>
       </div>
   </div>
  	</div>
  </div>
	</div>
</div>
<!-- END MAIN CONTENT -->
@stop