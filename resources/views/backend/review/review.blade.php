@extends('backend.layouts.main',['title'=>'test'])
@section('title', 'Review')
@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.12/r-2.1.0/datatables.min.css"/>
@stop
@section('content')
<div class="breadcrumbs">
  <h1>@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- BEGIN MAIN CONTENT -->
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-star"></i>
					<span class="caption-subject bold uppercase">@yield('title') list</span>
				</div>
			</div>
			<div id="review" class="portlet-body">
				<table id="review-table" class="table table-striped" cellspacing="0" width="100%">
					<thead>
						<tr>
							<td>Author</td>
							<td width="43%">Comment</td>
							<td>Rating</td>
							<td>Date Created</td>
							<td class="text-center">Actions</td>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- END MAIN CONTENT -->
@stop
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.12/r-2.1.0/datatables.min.js"></script>
<script>
	$(document).on('click', '.reject', function(e){
		e.preventDefault();
		var url = $(this).attr('href');
		swal({   title: "Are you sure?",   
				 text: "You will not be able to recover this comment!",
				 type: "warning",   
				 showCancelButton: true,   
				 confirmButtonColor: "#DD6B55",   
				 confirmButtonText: "Yes, reject it!",   
				 closeOnConfirm: false,
				 showLoaderOnConfirm: true,
			}, 
				 function(){
				 	$.ajax({
				 		url: url,
				 		type: 'DELETE',
				 		success: function() {
				 			swal.close();
				 			$('#review-table').dataTable().api().draw()
				 		}
				 	})
				 });
	});

	$(document).on('click', '.approve', function(e){
		e.preventDefault();
		var url = $(this).attr('href');
		swal({   title: "Approve comment?",   
				 type: "warning",   
				 showCancelButton: true,   
				 confirmButtonColor: "#3598DC",   
				 confirmButtonText: "Yes, approve it!",   
				 closeOnConfirm: false,
				 showLoaderOnConfirm: true,
			}, 
				 function(){
				 	$.ajax({
				 		url: url,
				 		type: 'POST',
				 		success: function() {
				 			swal.close();
				 			$('#review-table').dataTable().api().draw()
				 		}
				 	})
				 });
	});

	$('#review-table').DataTable({
		processing: true,
        serverSide: true,
        responsive: true,
        ajax: '/admin/review/data',
        columns: [
        	{ data: 'author', name: 'author' },
        	{ data: 'comment', name: 'comment', 'sortable' : false },
        	{ data: 'stars', name: 'stars' ,'sortable' : false, searchable: false },
        	{ data: 'created_at', name: 'created_at', searchable: false },
        	{ data: 'action','className': 'text-center', name: 'action', orderable: false,searchable: false },
        ],
		"oLanguage": {
			"sLengthMenu": "Entries per page: _MENU_",
		},
	});

	$('<button id="refresh" type="button" class="btn btn-default"> <i class="fa fa-refresh" aria-hidden="true"></i> Refresh</button>').appendTo('div.dataTables_filter');

	$('#refresh').click(function(){
		$('#review-table').dataTable().api().draw();
	});
</script>
@stop