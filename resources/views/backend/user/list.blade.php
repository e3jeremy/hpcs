@extends('backend.layouts.main',['title' => 'user.list'])
@section('title', 'Users')
@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.12/r-2.1.0/datatables.min.css"/>
@stop
@section('content')
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>User List</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">User List</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<div class="row">
<div class="col-md-12">
  <!-- BEGIN SAMPLE FORM PORTLET-->
  <div class="portlet box blue">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-user"></i>
        <span class="caption-subject bold uppercase"> User List</span>
      </div>
    </div>
    <div class="portlet-body">
      <table id="users-table" class="table table-striped" cellspacing="0" width="100%">
        <thead>
          <tr>
            <td>Sr.#</td>
            <td>Email</td>
            <td>Money</td>
            <td>Payment Request</td>
            <td>Request Date</td>
            <td>Date Created</td>
            <td>Actions</td>
          </tr>
        </thead>
      </table>
    </div>
  </div>
  <!-- END SAMPLE FORM PORTLET-->
  <!-- BEGIN SAMPLE FORM PORTLET-->
</div>
</div>
@stop
@section('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.12/r-2.1.0/datatables.min.js"></script>
<script>
  $('#users-table').DataTable({
    processing: true,
    serverSide: true,
    responsive: true,
    ajax: '/admin/user/data',
    "order": [[ 5, "desc" ]],
    columns: [
      { data: 'id', name: 'id' },
      { data: 'email', name: 'email' },
      { data: 'money', name: 'money' , searchable: false },
      { data: 'payment', name: 'payment', searchable: false },
      { data: 'request-date', name: 'request-date', searchable: false },
      { data: 'created_at', name: 'created_at', searchable: false },
      { data: 'action','className': 'text-center', name: 'action', orderable: false,searchable: false, 'width': '15%' },
    ],
    "oLanguage": {
      "sLengthMenu": "Entries per page: _MENU_",
    },
  });
  $('<button id="refresh" type="button" class="btn btn-default"> <i class="fa fa-refresh" aria-hidden="true"></i> Refresh</button>').appendTo('div.dataTables_filter');

  $('#refresh').click(function(){
    $('#users-table').dataTable().api().draw();
  });
</script>
@stop