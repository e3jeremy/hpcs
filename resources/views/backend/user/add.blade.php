@extends('backend.layouts.main',['title' => 'user.create'])
@section('content')
@section('title', 'Add User')
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>Add User</h1>
  <ol class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">Add User</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN CREATE USER ROW -->
<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-user"></i>
          <span class="caption-subject bold uppercase"> Add User Form</span>
        </div>
      </div>
      <div class="portlet-body">
        <div class="row">
          <div class="col-md-6">
            <form role="form">
              <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" class="form-control input-lg" placeholder="Email Address" required>
              </div>
              <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control input-lg" placeholder="Password" required>
              </div>
              <div class="form-group">
                <label>Money</label>
                <input type="number" name="money" class="form-control input-lg" placeholder="Money" required min="1" max="100">
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="portlet box border-dashed">
  <div class="portlet-body">
      <button type="button" class="btn btn-lg blue">
        <i class="fa fa-save"></i>  Save
      </button>
    </form>
  </div>
</div>
<!-- END CREATE USER ROW -->
@stop