@extends('backend.layouts.main')
@section('content')
@section('title', 'Good Comment List')
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>Good Comment List</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">Good Comment List</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN MAIN CONTENT -->
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
			<div class="portlet-title">
      <div class="caption">
        <i class="fa fa-barcode"></i>
        <span class="caption-subject bold uppercase"> Good Comment List</span>
      </div>
      <div class="actions">
      	<a href="/admin/comment/create" class="btn btn-default">
      	 <i class="fa fa-plus"></i>	Add Good Comment
      	</a>
      </div>
    </div>
    <div class="portlet-body">
    	<table class="table table-hover">
    		<thead>
    			<tr>
    				<th>Sr.#</th>
    				<th>User</th>
    				<th>Email</th>
    				<th>Survey</th>
    				<th>Status</th>
    				<th>Date Created</th>
    				<th>Actions</th>
    			</tr>
    		</thead>
    		<tbody>
    		@for($i = 1; $i <= 10; $i++)
					<tr>
    				<td>{{ $i }}</td>
    				<td>sree</td>
    				<td>admin@info.com</td>
    				<td>Third Survey</td>
    				<td><span class="label label-sm label-success"> Approved </span></td>
    				<td>2 years ago</td>
    				<td></td>
    			</tr>
    		@endfor
    		</tbody>
    	</table>
    	<div class="text-right">
			  <ul class="pagination pagination-sm">
			    <li>
			      <a href="javascript:;">
			      <i class="fa fa-angle-left"></i>
			      </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 1 </a>
			    </li>
			    <li class="active">
			      <a href="javascript:;"> 2 </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 3 </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 4 </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 5 </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 6 </a>
			    </li>
			    <li>
			      <a href="javascript:;">
			      <i class="fa fa-angle-right"></i>
			      </a>
			    </li>
			  </ul>
			</div>
    </div>
		</div>
	</div>
</div>
<!-- END MAIN CONTENT -->
@stop