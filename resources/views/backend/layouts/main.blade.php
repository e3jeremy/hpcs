<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<title>HPCS | @yield('title')</title>

	<meta name="_csrftoken" content="{{ csrf_token() }}">

	<!-- Begin Favicons for html and mobile browsers -->
        {{--<link rel="apple-touch-icon" sizes="76x76" href="/images/favicons/apple-touch-icon.png">--}}
        {{--<link rel="icon" type="image/png" href="/images/favicons/favicon-32x32.png" sizes="32x32">--}}
        {{--<link rel="icon" type="image/png" href="/images/favicons/favicon-16x16.png" sizes="16x16">--}}
        <link rel="manifest" href="/manifest.json">
        <link rel="mask-icon" href="/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="apple-mobile-web-app-title" content="HighPayingCashSurveys">
        <meta name="application-name" content="HighPayingCashSurveys">
        <meta name="theme-color" content="#ffffff">
    <!-- End Favicons for html and mobile browsers -->

	<!-- BEGIN MANDATORY LAYOUTS AND STYLES -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="/assets/css/simple-line-icons/simple-line-icons.min.css">
	<link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
   	<link rel="stylesheet" href="/assets/css/bootstrap-switch.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	<!-- END MANDATORY LAYOUTS AND STYLES -->
	<!-- BEGIN PAGE LEVEL LAYOUTS -->
    <link href="/assets/css/profile.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.12/r-2.1.0/datatables.min.css"/>

	<style>
		li.active{
			background-color: white;
		}
	</style>

	@yield('styles')
	<!-- END PAGE LEVEL LAYOUTS -->
	<!-- BEGIN THEME GLOBAL STYLES -->
  <link rel="stylesheet" href="/assets/css/bootstrap-fileinput.css">
  <link href="/assets/css/global/components.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="/assets/css/global/plugins.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
	<!-- BEGIN THEME LAYOUT -->
	<link rel="stylesheet" type="text/css" href="/assets/css/layout.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/custom.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<!-- END THEME LAYOUT -->
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo">
	<div class="wrapper">
		@include('backend.layouts._header')
		<div class="container-fluid">
			<div class="page-content">
				@yield('content')
			</div>
			<!-- BEGIN FOOTER -->
       <p class="copyright">
       	<?php echo date("Y") ?> © <a href="javascript:;">Highpayingcash Surveys.com</a>
       </p>
       <a href="#index" class="go2top">
          <i class="icon-arrow-up"></i>
       </a>
     <!-- END FOOTER -->
		</div>
	</div>
<script src="/js/backend/bootstrap.js"></script>
	<!--[if lt IE 9]>
		<script src="/assets/js/respond.min.js"></script>
		<script src="/assets/js/excanvas.min.js"></script>
	<![endif]-->
<script src="/js/backend/app.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.12/r-2.1.0/datatables.min.js"></script>

@yield('scripts')

@include('flash::message')

</body>
</html>
