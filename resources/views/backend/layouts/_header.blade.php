<!-- BEGIN HEADER -->
<header class="page-header">
  <nav class="navbar mega-menu" role="navigation">
    <div class="container-fluid">
      <div class="clearfix navbar-fixed-top">
        <!-- Brand and toggle get grouped for better mobile display -->
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="toggle-icon">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </span>
        </button>
        <!-- End Toggle Button -->
        <!-- BEGIN LOGO -->
        <a id="index" class="page-logo" href="/admin/dashboard">
        <img src="/images/logo.png" alt="Logo"> </a>
        <!-- END LOGO -->
        <!-- BEGIN SEARCH -->
      {{--  <form class="search" action="extra_search.html" method="GET">
          <input type="name" class="form-control" name="query" placeholder="Search Survey Name...">
          <a href="javascript:;" class="btn submit md-skip">
          <i class="fa fa-search"></i>
          </a>
        </form>--}}
        <!-- END SEARCH -->
        <!-- BEGIN TOPBAR ACTIONS -->
        <div class="topbar-actions">
          <!-- BEGIN USER PROFILE -->
          <div class="btn-group-img btn-group">
            <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <span>Welcome, User</span>
            <img src="/images/profile.jpg" alt=""> </button>
            <ul class="dropdown-menu-v2" role="menu">
              <li>
                <a href="/admin/profile">
                <i class="icon-user"></i> My Profile
                </a>
              </li>
              <li class="divider"> </li>
              <li>
                <a href="/admin/logout">
                <i class="icon-key"></i> Log Out </a>
              </li>
            </ul>
          </div>
          <!-- END USER PROFILE -->
        </div>
        <!-- END TOPBAR ACTIONS -->
      </div>
      <!-- BEGIN HEADER MENU -->
      <div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
        <ul class="nav navbar-nav">
          <li class="dropdown dropdown-fw {{ Request::is( 'admin/dashboard' ) ? 'open' : '' }}">
            <a href="/admin/dashboard" class="text-uppercase">
            <i class="icon-home"></i> Dashboard</a>
          </li>
          <li class="dropdown dropdown-fw {{ Request::is( 'admin/survey/*' ) || Request::is( 'admin/survey' ) ? 'open' : '' }}">
            <a href="javascript:;" class="text-uppercase">
            <i class="fa fa-road"></i> Surveys </a>
            <ul class="dropdown-menu dropdown-menu-fw">
              <li class="{{ $title == 'survey.list.create'?'active':'' }}">
                <a href="/admin/survey/create">
                <i class="fa fa-plus"></i> Add Survey </a>
              </li>
              <li class="{{ $title == 'survey.list'?'active':'' }}">
                <a href="/admin/survey">
                <i class="fa fa-list-alt" aria-hidden="true"></i> Survey List (Default and 30 days challenge)</a>
              </li>
              <li class="{{ $title == 'survey.list.local'?'active':'' }}">
                <a href="/admin/survey/local">
                <i class="fa fa-list-alt" aria-hidden="true"></i> Survey List (Local Search)</a>
              </li>
            </ul>
          </li>
          <li class="dropdown dropdown-fw  {{ Request::is( 'admin/question' ) || Request::is( 'admin/question/*' ) ? 'open' : '' }}">
            <a href="javascript:;" class="text-uppercase">
            <i class="fa fa-question"></i> Survey Question </a>
            <ul class="dropdown-menu dropdown-menu-fw">
              <li class="{{ $title == 'survey.question.create'?'active':'' }}">
                <a href="/admin/question/create"><i class="fa fa-plus"></i> Add Survey Question </a>
              </li>
              <li class="{{ $title == 'survey.question.list'?'active':'' }}">
                <a href="/admin/question"><i class="fa fa-list-alt" aria-hidden="true"></i> Survey Question List </a>
              </li>
              <li class="{{ $title == 'survey.coreg.create'?'active':'' }}">
                <a href="/admin/coreg/create"><i class="fa fa-plus" aria-hidden="true"></i> Add Co-Reg </a>
              </li>
              <li class="{{ $title == 'survey.coreg.list'?'active':'' }}">
                <a href="/admin/coreg"><i class="fa fa-plus" aria-hidden="true"></i> Manage Co-Reg </a>
              </li>
            </ul>
          </li>
          <li class="dropdown dropdown-fw  {{ Request::is( 'admin/user' ) || Request::is( 'admin/user/*' ) ? 'open' : '' }}">
            <a href="javascript:;" class="text-uppercase">
            <i class="fa fa-user"></i> User </a>
            <ul class="dropdown-menu dropdown-menu-fw">
              <li class="{{ $title == 'user.create'?'active':'' }}">
                <a href="/admin/user/create"><i class="fa fa-plus"></i> Add User </a>
              </li>
              <li class="{{ $title == 'user.list'?'active':'' }}">
                <a href="/admin/user"><i class="fa fa-list-alt"></i> User List </a>
              </li>
            </ul>
          </li>
        {{--  <li class="dropdown dropdown-fw {{ Request::is( 'admin/zipcode' ) || Request::is( 'admin/zipcode/*' ) ? 'open' : '' }}">
            <a href="javascript:;" class="text-uppercase">
            <i class="fa fa-barcode"></i> Zip Code </a>
            <ul class="dropdown-menu dropdown-menu-fw">
              <li>
                <a href="/admin/zipcode/create"><i class="fa fa-plus"></i> Add Zip Code </a>
              </li>
              <li>
                <a href="/admin/zipcode"><i class="fa fa-list-alt"></i> Zip Code List </a>
              </li>
            </ul>
          </li>--}}
         {{-- <li class="dropdown dropdown-fw {{ Request::is( 'admin/comment' ) || Request::is( 'admin/comment/create' ) ? 'open' : '' }}">
            <a href="javascript:;" class="text-uppercase">
            <i class="fa fa-comments"></i> Good Comments </a>
            <ul class="dropdown-menu dropdown-menu-fw">
              <li>
                <a href="/admin/comment/create"><i class="fa fa-plus"></i> Add Good Comments </a>
              </li>
              <li>
                <a href="/admin/comment"><i class="fa fa-list-alt"></i> Good Comments List </a>
              </li>
            </ul>
          </li>
          <li class="dropdown dropdown-fw {{ Request::is( 'admin/feedback' ) || Request::is( 'admin/feedback/*' ) ? 'open' : '' }}">
            <a href="javascript:;" class="text-uppercase">
            <i class="fa fa-comment-o" aria-hidden="true"></i> Feed Back </a>
            <ul class="dropdown-menu dropdown-menu-fw">
              <li>
                <a href="/admin/feedback/create"><i class="fa fa-plus"></i> Add Feed Back </a>
              </li>
              <li>
                <a href="/admin/feedback"><i class="fa fa-list-alt"></i> Feed Back List </a>
              </li>
            </ul>
          </li>
          <li class="dropdown dropdown-fw {{ Request::is( 'admin/comment/bad' ) || Request::is( 'admin/comment/filter' ) ? 'open' : '' }}">
            <a href="javascript:;" class="text-uppercase">
            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Bad Comments </a>
            <ul class="dropdown-menu dropdown-menu-fw">
              <li>
                <a href="/admin/comment/bad"><i class="fa fa-list-alt"></i> Bad Comments List </a>
              </li>
              <li>
                <a href="/admin/comment/filter"><i class="fa fa-filter" aria-hidden="true"></i> Bad Comments Filter </a>
              </li>--}}
            </ul>
          </li>
        </ul>
      </div>
      <!-- END HEADER MENU -->
    </div>
    <!--/container-->
  </nav>
</header>
<!-- END HEADER -->