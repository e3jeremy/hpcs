@extends('backend.layouts.main',['title' => 'survey.question.list'])
@section('content')
@section('title', 'Survey Question List')
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>Survey Question List</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">Survey Question List</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN SAMPLE FORM PORTLET-->
	  <div class="portlet box blue">
	  	<div class="portlet-title">
      <div class="caption">
        <i class="fa fa-road"></i>
        <span class="caption-subject bold uppercase">Survey Question List</span>
      </div>
      <div class="actions">
      	<a href="/admin/question/create" class="btn btn-default">
      	 <i class="fa fa-plus"></i>	Add Question
      	</a>
      </div>
    </div>
    <div class="portlet-body">
    	<table id="questions-table" class="table table-hover">
    		<thead>
    			<tr>
    				<th>Sr.#</th>
    				<th>Survey</th>
					<th>Title</th>
    				<th>Question</th>
    				<th>Question Order</th>
    				<th>Date Created</th>
    				<th>Action</th>
    			</tr>
    		</thead>
    	</table>
    </div>
	  </div>
	  <!-- END FORM PORTLET -->
	</div>
</div>


<!-- Large modal -->

<div id="questions-modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Title - @{{ questionTitle }}</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">

						<table class="table">
							<tr>
								<td>Question id</td>
								<td v-if="canEdit"><input class="form-control" type="text" name="" value="@{{ questionId  }}"> </td>
								<td v-else>@{{ questionId }}</td>
							</tr>
							<tr>
								<td>Title</td>
								<td v-if="canEdit"><input class="form-control" type="text" name="" value="@{{ questionTitle  }}"> </td>
								<td v-else>@{{ questionTitle }}</td>
							</tr>
							<tr>
								<td>Question</td>
								<td v-if="canEdit"><textarea class="form-control" type="text" name=""> @{{ questionContent }} </textarea> </td>
								<td v-else>@{{ questionContent }}</td>
							</tr>
							<tr></tr>
							<tr>
								<td>Label</td>
								<td v-if="canEdit"><input class="form-control" type="text" name="" value="@{{ questionLabel  }}"> </td>
								<td v-else>@{{ questionLabel }}</td>
							</tr>
							<tr>
								<td>Order</td>
								<td v-if="canEdit"><input class="form-control" type="text" name="" value="@{{ questionOrder  }}"> </td>
								<td v-else>@{{ questionOrder }}</td>
							</tr>
							<tr></tr>
							<tr>
								<td>Lead Reactor</td>
								<td v-if="canEdit"><input class="form-control" type="text" name="" value="@{{ questionLeadReactor  }}"> </td>
								<td v-else>@{{ questionLeadReactor }}</td>
							</tr>
							<tr>
								<td>Polar</td>
								<td v-if="canEdit"><input class="form-control" type="text" name="" value="@{{ questionPolar  }}"> </td>
								<td v-else>@{{ questionPolar }}</td>
							</tr>
							<tr>
								<td>Date Created</td>
								<td v-if="canEdit"><input class="form-control" type="text" name="" value="@{{ questionCreated  }}"> </td>
								<td v-else>@{{ questionCreated }}</td>
							</tr>
							<tr>
								<td>Date Updated</td>
								<td v-if="canEdit"><input class="form-control" type="text" name="" value="@{{ questionCreated  }}"> </td>
								<td v-else>@{{ questionCreated }}</td>
							</tr>
						</table>

					</div>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" v-if="canEdit" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>




@stop

@section('scripts')

	<script>


		new Vue({

			el:'body',

			data : {

				dataTable : null,
				questions : [],
				canEdit : false,
				questionId : 0,
				questionLabel : null,
				questionTitle : null,
				questionContent : null,
				questionOrder : null,
				questionLeadReactor : null,
				questionPolar : null,
				questionCreated : null,
				questionUpdated : null,
				questionMimic : null,
			},


			ready : function(){
				this.initialize();
			},

			methods : {

				initialize : function(){
					var self = this;

					this.dataTable = $('#questions-table').DataTable
					({
						processing: true,
						serverSide: true,
						responsive: true,
						ajax: '/admin/question/data',
						columns: [
							{ data: 'id', name: 'id' },
							{ data: 'label', name: 'label' },
							{ data: 'title', name: 'title' },
							{ data: 'content', name: 'content' },
							{ data: 'order', name: 'order' },
							{ data: 'created_at', name: 'created_at', searchable: false },
							{ data: 'action','className': 'text-center', name: 'action', orderable: false,searchable: false, 'width': '15%' },
						]
					});



					$('#questions-table').on('click','.view-question', function () {

						self.canEdit = false;
						self.questionLabel = $(this).attr('data-label');
						self.questionTitle = $(this).attr('data-title');
						self.questionContent = $(this).attr('data-content');
						self.questionOrder = $(this).attr('data-order');
						self.questionLeadReactor = $(this).attr('data-lead_reactor');
						self.questionPolar = $(this).attr('data-polar');
						self.questionCreated = $(this).attr('data-created');
						self.questionUpdated = $(this).attr('data-updated');

						self.mimic = $(this).attr('data-mimic');
						self.questionId = $(this).attr('data-id');

						self.$nextTick(function(){
							$('#questions-modal').modal('show');
						});
					});

					$('#questions-table').on('click','.edit-question', function () {

						self.canEdit = true;
						self.questionLabel = $(this).attr('data-label');
						self.questionTitle = $(this).attr('data-title');
						self.questionContent = $(this).attr('data-content');
						self.questionOrder = $(this).attr('data-order');
						self.questionLeadReactor = $(this).attr('data-lead_reactor');
						self.questionPolar = $(this).attr('data-polar');
						self.questionCreated = $(this).attr('data-created');
						self.questionUpdated = $(this).attr('data-updated');

						self.mimic = $(this).attr('data-mimic');
						self.questionId = $(this).attr('data-id');

						self.$nextTick(function(){
							$('#questions-modal').modal('show');
						});
					});


				}

			}

		});













	</script>
@stop


