@extends('backend.layouts.main',['title' => 'survey.list.create'])
@section('content')
@section('title', 'Add Survey')
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>Add Survey</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">Add Survey</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-road"></i>
          <span class="caption-subject bold uppercase"> Add Survey Form</span>
        </div>
      </div>
      <div class="portlet-body">
        <form id="form-offer">
          <div class="row form-group">
            <div class="col-md-6">
              <label for="">Creative ID</label>
              <input name="id" type="text" class="form-control" required>
            </div>
            <div class="col-md-6">
            </div>
          </div>

          <div class="row form-group">
            <div class="col-md-6">
              <div class="row form-group">
                <div class="col-md-12">
                  <label for="">Offer Name</label>
                  <input name="name" type="text" class="form-control" required>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <label for="">CD1 Link</label>
                  <input name="link" type="text" class="form-control" required>
                </div>
              </div>



            </div>
            <div class="col-md-6">
              <label for="">Rewards to Promote</label>
              <textarea class="form-control" name="description" id="" rows="5" style="resize: vertical"></textarea>
            </div>
          </div>

          <hr>

          <div class="row form-group">
            <div class="col-md-6">
              <label for="">Categories Type</label>
              <div class="row">
                <div class="col-md-2">
                  <input type="checkbox" name="has_cash" value="1"> Cash
                </div>
                <div class="col-md-2">
                  <input type="checkbox" name="has_points" value="1"> Points
                </div>
                <div class="col-md-3">
                  <input type="checkbox" name="has_sweeptakes" value="1"> Sweepstakes
                </div>
                <div class="col-md-2">
                  <input type="checkbox" name="has_coupons" value="1"> Coupons
                </div>
                <div class="col-md-3">
                  <input type="checkbox" name="has_freebie" value="1"> Freebies
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <label for="">Incent Type</label>
              <div class="row">
                <div class="col-md-6">
                  <input name="is_hpcs" value="0" type="radio" checked> Advertiser
                </div>
                <div class="col-md-6">
                  <input name="is_hpcs" value="1" type="radio"> EIQ
                </div>
              </div>
            </div>
          </div>

          <hr>

          <div class="row form-group">
            <div class="col-md-6">
              <label for="">Incentives Type</label>
              <div class="row">
                <div class="col-md-4">
                  <input name="is_type" type="radio" value="1"> Sponsor
                  <input name="cash" type="text" value="0.00">
                </div>
                <div class="col-md-4">
                  <input name="is_type" type="radio" value="0"> Freebies
                </div>
                <div class="col-md-4">
                  <input type="checkbox" name="is_hpcs" value="1"> HPCS
                  <input name="hpcs_cash" type="text" value="0.00">
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <label for="">Status</label>
              <div class="row">
                <div class="col-md-6">
                  <input name="is_status" type="radio" checked value="1"> Active
                </div>
                <div class="col-md-6">
                  <input name="is_status" type="radio" value="2"> Inactive
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div><!-- end col-md-12 -->
</div><!-- end row -->
<div class="portlet box border-dashed">
  <div class="portlet-body">
    <button @click="offerSave" type="button" class="btn btn-lg blue">
      <i class="fa fa-save"></i>  Save
    </button>
    </form>
  </div>
</div>
@stop

@section('scripts')
<script src="/js/backend/common.js"></script>

<script type="text/javascript">

  new Vue ({

    el: 'body',

    data: {


    },
    ready: function () {

    },

    methods: {

      offerSave : function () {

        var data = $('#form-offer').serializeArray();

        $.ajax({
          url:'/admin/offers/store',
          method:'POST',
          data: data
        }).success(function(response) {
          notify("Success!", "Offer has been inserted", "success");
          $('#form-offer').find("input[type=text], textarea").val("");
          console.log(response);
        }).fail(function(e) {
          notify(e.statusText || ($.parseJSON(e.responseText)).message, "", "error", 'overlay');
        });


      }
    }
  });

</script>

@endsection