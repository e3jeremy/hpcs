@extends('backend.layouts.main',['title' => 'survey.list'])
@section('content')
@section('title', 'Survey List')

@section('styles')
	<style>
		.invisible {
			display: none
		}

		.bodyDesignation {
			border: 2px solid beige;
		}

		.activeDesignation {
			background-color : #eaacac !important;
		}

		/*.activeDesignation > tr{*/
			/*border-left-style: dashed;*/
			/*border-right-style: dashed;*/
		/*}*/

		.activeDesignation:hover {
			background-color : #eaacac !important;
		}

		.hide-me{
			 display:none;
			 opacity:0;
			 transition:opacity 1.0s linear;
		 }

	</style>


@stop
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>Survey List</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">Add Survey</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<div class="row v-cloak--hidden">
	<div class="col-md-12">
		<!-- BEGIN SAMPLE FORM PORTLET-->
	  <div class="portlet box blue">
	  	<div class="portlet-title">
      <div class="caption">
        <i class="fa fa-road"></i>
        <span class="caption-subject bold uppercase"> Survey List <small>(Default and 30 days challenge)</small></span>
      </div>
      <div class="actions">
      	<a href="/admin/survey/create" class="btn btn-default">
      	 <i class="fa fa-plus"></i>	Add Survey
      	</a>
      </div>
    </div>
    <div class="portlet-body">
			<table id="surveys-table" class="table table table-striped" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Sr. #</th>
						<th>Page Name</th>
						<th>Header</th>
						<th>Survey Type</th>
						<th>Position</th>
						<th>Search Type</th>
						<th>Paid</th>
						<th>Featured</th>
						<th>Rating</th>
						<th>Amount</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody v-for="designation in designations" class="bodyDesignation">
					<tr @click="myChildQuestions($index+1)" style="cursor: pointer;" :class="$index+1==childToggledId?'activeDesignation':''">
						<td> <span><i v-if="$index+1==childToggledId" class="fa fa-folder-open-o" aria-hidden="true"></i><i v-else class="fa fa-folder-o" aria-hidden="true"></i></span> @{{ $index+1 }}</td>
						<td> @{{ designation.label  }}</td>
						<td> @{{ designation.header_big  }}</td>
						<td> @{{ designation.surveys.label }}</td>
						<td> @{{ designation.order  }}</td>
						<td> n/a  </td>
						<td> @{{ designation.price | paid  }}</td>
						<td> n/a </td>
						<td> n/a </td>
						<td> @{{ designation.price | currency  }}</td>
						<td> n/a </td>
					</tr>
						<tr :class="$index+1==childToggledId?'':'hide-me'" style="background-color: floralwhite">
						<td colspan="11">
							<div class="row">
								<div class="col-md-4">
									<label for="">Header Page Title :</label>
									<input type="text" class="form-control designation-title" placeholder="Page Title" value="@{{ designation.header_big }}">
								</div>
								<div class="col-md-2">
									<label for="">Header Small:</label>
									<input type="text" class="form-control designation-title-small" placeholder="" value="@{{ designation.header_small  }}">
								</div>

								<div class="col-md-2">
									<label for="">Reward Prize:</label>
									<input type="text" class="form-control designation-price" placeholder="$ 1.00" value="@{{ designation.price  }}" disabled="disabled">
								</div>


								<div class="col-md-2">
									<label for="">Questions Max Limit :</label>
									{{--<input type="text" class="form-control" placeholder="limit" value="@{{ designation.no_of_questions  }}">--}}
									<select name="" id="" class="form-control designation-max">
										<option v-for="n in 12" value="@{{ n+1 }}" :selected="designation.no_of_questions==(n+1)"> @{{ n+1 }} </option>
									</select>
								</div>

								<div class="col-md-1">
									<label for=""> Action </label>
									<button @click="updateDesignation($event,designation.id)" class="btn btn-success form-control"> <span v-if="ajaxLoadingForUpdateDesignation==designation.id"><i class="fa fa-spinner fa-spin"></i></span><span v-else> <i class="fa fa-floppy-o" aria-hidden="true"></i> Save</span></button>
								</div>

							</div>


							<hr>

							<div class="row">
								<div class="col-md-12">
									<button @click="createNormalQuestions(designation.id)" class="btn btn-xs btn-primary"><i class="fa fa-plus-square" aria-hidden="true"></i>  Normal Question</button>
									<button @click="attachCoregQuestions(designation.id)" class="btn btn-xs btn-info"><i class="fa fa-plus-square" aria-hidden="true"></i>  Coreg Question</button>
									<button @click="attachLinkoutsQuestions(designation.id)" class="btn btn-xs btn-success"><i class="fa fa-plus-square" aria-hidden="true"></i>  Linkout Question</button>
									<label for="">Current questions: @{{ designation.questions.length }}</label>
								</div>
							</div>

							<br>

							<div class="row">
								<div class="col-md-4"><label for="">Survey Question Lists</label></div>
							</div>

							<table class="table" cellspacing="0" width="100%" style="background: cornsilk; border: 1px solid wheat;">
								<thead style="background-color: bisque">
								<th width="5%">Active</th>
								<th width="5%">Sr. #</th>
								<th width="10%">Type</th>
								<th width="20%">Title</th>
								<th width="25%">Content</th>
								<th width="10%">Position</th>
								<th width="10%">Created At</th>
								<th width="15%">Action</th>
								</thead>
								<tbody v-for="question in designation.questions">
									<td><input type="checkbox" :checked="question.status==1"></td>
									<td>@{{ $index+1 }}</td>
									<td>@{{ question | coregFilter }}</td>
									<td><span v-if="question.id==questionToggledId"><input class="form-control question-title" type="text" value="@{{ question.title }}"></span> <span v-else>@{{ question.title }}</span></td>
									<td><span v-if="question.id==questionToggledId"><textarea class="form-control question-content" style="resize: vertical;">@{{ question.content  }}</textarea></span> <span v-else>@{{ question.content }}</span></td>
									<td>@{{ question.order }}</td>
									<td>@{{ question.created_at }}</td>
									<td>
										<button v-if="question.id==questionToggledId" @click="questionToggled(question.id)" class="btn btn-xs btn-warning"><i class="fa fa-undo" aria-hidden="true"></i> </button>
										<button v-else @click="questionToggled(question.id)" class="btn btn-xs btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></button>
										<button @click="updateQuestionNew($event,question.id)" class="btn btn-xs btn-success" :disabled="!(question.id==questionToggledId) || question.id==ajaxLoading"><span></span><i v-if="question.id==ajaxLoading"  class="fa fa-spinner fa-spin"></i><i v-else class="fa fa-save" aria-hidden="true"></i></button>
										<button @click="deleteQuestion(question.id,question.lead_reactor)" class="btn btn-xs btn-danger pull-right"><i class="fa fa-times" aria-hidden="true"></i></button>
									</td>
								</tbody>
							</table>
							<hr>
							{{--<button class="btn btn-success pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i>  Save</button>--}}
						</td>
					</tr>
				</tbody>
			</table>
			</div>
	  <!-- END SAMPLE FORM PORTLET -->
		</div>
	</div>
</div>



<div id="createNormalQuestionsModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Create New Normal Questions</h4>
			</div>
			<div class="modal-body">

				<form id="newNormalQuestionForm">
					{{ csrf_field() }}
					<div class="row form-group">
						<div class="col-md-12">
							<label for="">New Title</label>
							<input name="title" type="text" class="form-control" placeholder="Place the Title here...">
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-12">
							<label for="">New Content</label>
							<textarea name="content" type="text" class="form-control"  placeholder="Place the Content here..." style="resize: vertical;"></textarea>
						</div>
					</div>

					<div class="row form-group">
						<div class="col-md-12">
							<label for="">Polar Type</label>
							<select name="polar" id="">
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
				<button @click="saveNormalQuestion" type="button" class="btn btn-primary" :disabled="ajaxLoadingForNewQuestions==true"><span v-if="ajaxLoadingForNewQuestions==false" >Save changes</span> <span v-else><i class="fa fa-spinner fa-spin"></i> Saving</span></button>
			</div>
		</div>
	</div>
</div>

<div id="attachCoregQuestionsModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Attach Coregs Campaign</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<table class="table" cellspacing="0" width="100%" >
							<thead>
								<th width="5%">Sr#</th>
								<th width="20%">Image</th>
								<th width="15%">Name</th>
								<th width="15%">Deal</th>
								<th width="15%">Type</th>
								<th width="15%">Price</th>
								<th width="15%">Action</th>
							</thead>
							<tbody v-if="campaigns">
								<tr v-for="coreg in campaigns.data">
									<td>@{{ coreg.id }}</td>
									<td><image :src="coreg.image | imageFormatter" width="100%"></image></td>
									<td>@{{ coreg.name }}</td>
									<td>@{{ coreg.deal }}</td>
									<td>@{{ filterCoregType(coreg.sponsor,coreg.freebie) }}</td>
									<td>@{{ coreg.price }} || @{{ coreg.published_in }}</td>
									<td><button @click="attachCoreg(coreg)" class="btn btn-primary" title="push this coreg to this designation">Attach <i class="fa fa-sign-in" aria-hidden="true"></i></button></td>
								</tr>
							</tbody>
							<tbody v-else>
								<tr style="text-align: center">
									<td colspan="7">Fetching campaigns data <i class="fa fa-refresh fa-spin"></i> </td>
								</tr>
							</tbody>
						</table>

						<hr>
						<nav aria-label="Page navigation"  style="margin: auto; text-align: center;">
							<ul class="pagination">
								<li @click="getCampaignPaginate(campaigns.prev_page)" class="page-item">
									<a class="page-link" href="javascript: void(0);" aria-label="Previous">
										<span aria-hidden="true">&laquo;</span>
										<span class="sr-only">Previous</span>
									</a>
								</li>

								<li @click="getCampaignPaginate(campaigns.url+''+(n+1))" class="page-item @{{ campaigns.current_page==(n+1) ? 'active' : '' }}" v-for="n in campaigns.length_items">
									<a class="page-link" href="javascript: void(0);">@{{ n+1 }}</a>
								</li>

								<li @click="getCampaignPaginate(campaigns.next_page)" class="page-item">
									<a class="page-link" href="javascript: void(0);" aria-label="Next">
										<span aria-hidden="true">&raquo;</span>
										<span class="sr-only">Next</span>
									</a>
								</li>
							</ul>
						</nav>
						<p style="margin: auto; text-align: center;">page @{{ campaigns.current_page }} out of @{{ campaigns.length_items }}</p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>


<div id="coregConfirmationModal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Finalizing Coreg Question</h4>
			</div>
			<div class="modal-body">
				<div class="row form-group">
					<div class="col-md-3">
						<label for=""><strong>Image</strong></label>
						<br>
						<img id="newCoregImage" style="width:100%;">
					</div>
					<div class="col-md-4">
						<label for=""><strong>Coreg Name</strong></label>
						<br>
						<label id="newCoregDeal" for=""></label>
					</div>
					<div class="col-md-5">
						<label for=""><strong>Coreg Description</strong></label>
						<br>
						<label id="newCoregDescription" for=""></label>
					</div>
				</div>
				<form id="newCoregQuestionForm">
					{{ csrf_field() }}
					<div class="row form-group">
						<div class="col-md-12">
							<label for="">Create Title</label>
							<input id="newCoregDealInput" name="title" type="text" class="form-control" placeholder="Place the Title here...">
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-12">
							<label for="">Create Content</label>
							<textarea id="newCoregDescriptionInput" name="content" type="text" class="form-control"  placeholder="Place the Content here..." style="resize: vertical;"></textarea>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button @click="cancelCoregConfirmation" type="button" class="btn btn-warning">Cancel</button>
				<button @click="saveCoregConfirmation" type="button" class="btn btn-primary" :disabled="ajaxLoadingForNewQuestions==true"><span v-if="ajaxLoadingForNewQuestions==false" >Insert Coreg</span> <span v-else><i class="fa fa-spinner fa-spin"></i> Saving</span></button>
			</div>
		</div>

	</div>
</div>




<div id="attachLinkoutsQuestionsModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Attach Link Outs Offers</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<table class="table" cellspacing="0" width="100%" >
							<thead>
							<th width="5%">Cid</th>
							<th width="20%">Name</th>
							<th width="15%">Cake</th>
							<th width="10%">Type</th>
							<th width="5%">Cash</th>
							<th width="10%">Hpcs</th>
							<th width="5%">Reward</th>
							<th width="10%">Action</th>
							</thead>
							<tbody v-if="offers">
							<tr v-for="offer in offers.data">
								<td>@{{ $index+1 }}</td>
								<td>@{{ offer.name }}</td>
								<td>@{{ offer.cake_id }}</td>
								<td>@{{ offer.is_sponsors | offerFilter }}</td>
								<td>@{{ offer.cash | currency }}</td>
								<td>@{{ offer.is_hpcs | hpcsFilter }}</td>
								<td>@{{ offer.hpcs_cash | currency }}</td>
								<td><button @click="attachLinkout(offer)" class="btn btn-primary" title="push this link out to this designation">Attach <i class="fa fa-sign-in" aria-hidden="true"></i></button></td>
							</tr>
							</tbody>
							<tbody v-else>
							<tr style="text-align: center">
								<td colspan="7">Fetching campaigns data <i class="fa fa-refresh fa-spin"></i> </td>
							</tr>
							</tbody>
						</table>

						<hr>
						{{--<nav aria-label="Page navigation"  style="margin: auto; text-align: center;">--}}
							{{--<ul class="pagination">--}}
								{{--<li @click="getCampaignPaginate(campaigns.prev_page)" class="page-item">--}}
								{{--<a class="page-link" href="javascript: void(0);" aria-label="Previous">--}}
									{{--<span aria-hidden="true">&laquo;</span>--}}
									{{--<span class="sr-only">Previous</span>--}}
								{{--</a>--}}
								{{--</li>--}}

								{{--<li @click="getCampaignPaginate(campaigns.url+''+(n+1))" class="page-item @{{ campaigns.current_page==(n+1) ? 'active' : '' }}" v-for="n in campaigns.length_items">--}}
								{{--<a class="page-link" href="javascript: void(0);">@{{ n+1 }}</a>--}}
								{{--</li>--}}

								{{--<li @click="getCampaignPaginate(campaigns.next_page)" class="page-item">--}}
								{{--<a class="page-link" href="javascript: void(0);" aria-label="Next">--}}
									{{--<span aria-hidden="true">&raquo;</span>--}}
									{{--<span class="sr-only">Next</span>--}}
								{{--</a>--}}
								{{--</li>--}}
							{{--</ul>--}}
						{{--</nav>--}}
						{{--<p style="margin: auto; text-align: center;">page @{{ campaigns.current_page }} out of @{{ campaigns.length_items }}</p>--}}
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>



@stop

@section('scripts')

	<script src="/js/backend/common.js"></script>
	<script type="text/javascript">

		new Vue ({

			el : 'body',

			data : {

				designations : [],
				campaigns : null,
				offers : [],
				childToggledId : 0,
				questionToggledId : 0,
				ajaxLoading : false,
				currentDesignationId : 0,
				currentCoregId : 0,
				ajaxLoadingForNewQuestions : false,
				ajaxLoadingForUpdateDesignation : false

			},

			ready : function(){
				this.initialize();
			},

			methods : {

				initialize : function () {
					var self = this;
					$.get('/admin/survey/designations',function (response) {
						self.designations = response.data;
					})
				},

				myChildQuestions : function(id) {

					if(this.childToggledId==id)
					{
						return this.childToggledId = 0;
					}
					return this.childToggledId = id;
				},

				childQuestionsDiv : function(id){

					if(id==this.childToggledId)
					{
						return;
					}
					return 'hidden';

				},

				filterCoregType : function(sponsor,freebie){

					if(sponsor==1)
					{
						return 'Sponsors';
					}

					else if(freebie==1)
					{
						return 'Freebies';
					}

					else {
						return 'N/A';
					}
				},

				questionToggled : function(id)
				{
					if(this.questionToggledId==id)
					{
						return this.questionToggledId = 0;
					}
					return this.questionToggledId = id;

				},

				updateQuestionNew : function(e,id){
					var self = this;
					var $this = $(e.target);
					self.ajaxLoading = id;

					var title = $this.closest('tr').find('.question-title').val();
					var content = $this.closest('tr').find('.question-content').val();

					var data = { id : id, title : title, contents : content,  _token: $('[name="_token"]').attr('content') };

					$.ajax({
						url:'/admin/question/update',
						method:'PATCH',
						data: data
					}).success(function(response) {
						notify("Success!", "Question has been updated", "success");
						self.questionToggledId = 0;
						self.ajaxLoading = 0;
//
						self.designations = response.data;
					}).fail(function(e) {
						notify(e.statusText || ($.parseJSON(e.responseText)).message, "", "error", 'overlay');
					});


//					confirmUpdateQuestions('/admin/question/update','Are you sure you want to update this?',data,function(response){
//						self.questionToggledId = 0;
//						self.ajaxLoading = 0;
//
//						self.designations = response.data;
//
//					});

				},

				createNormalQuestions: function (designationId) {
					this.currentDesignationId = designationId;
					$('#createNormalQuestionsModal').modal('show');

				},

				attachCoregQuestions: function (designationId) {
					this.currentDesignationId = designationId;
					this.getCampaignPaginate(null);

					$('#attachCoregQuestionsModal').modal('show');
				},

				attachLinkoutsQuestions: function (designationId) {
					this.currentDesignationId = designationId;
					this.getLinkoutsPaginate(null);

					$('#attachLinkoutsQuestionsModal').modal('show');
				},

				getLinkoutsPaginate : function(url)
				{
					var self = this;
//					self.campaigns=null;
					if(url==null || url==undefined)
					{
						url = '/admin/survey/offers/list';
					}
					$.get(url,{ active:true },function(response){
						var data = {
							data : response.data.data,
							current_page : response.data.current_page,
							next_page : response.data.next_page_url,
							prev_page : response.data.prev_page_url,
							length_items : response.data.last_page,
							total : response.data.total,
							url : '/admin/survey/offers/list?page='
						}
						self.offers = data;
					});
				},

				getCampaignPaginate : function(url)
				{
					var self = this;
					self.campaigns=null;
					if(url==null || url==undefined)
					{
						url = '/admin/campaign/paginate/list';
					}
					$.get(url,function(response){
						var data = {
							data : response.data.data,
							current_page : response.data.current_page,
							next_page : response.data.next_page_url,
							prev_page : response.data.prev_page_url,
							length_items : response.data.last_page,
							total : response.data.total,
							url : '/admin/campaign/paginate/list?page='
						}
						self.campaigns = data;
					});
				},

				saveNormalQuestion : function(){

					var self = this;

					self.ajaxLoadingForNewQuestions = true;

					var data = $('#newNormalQuestionForm').serializeArray();
					data.push({ name: 'designation_id', value: this.currentDesignationId });

					$.ajax({
						url:'/admin/question/store',
						method:'POST',
						data: data
					}).success(function(response) {
						notify("Success!", "Question has been updated", "success");
						$('#newNormalQuestionForm').find("input[type=text], textarea").val("");
						self.designations = response.data;
					}).fail(function(e) {
						notify(e.statusText || ($.parseJSON(e.responseText)).message, "", "error", 'overlay');
					}).done(function(){
						self.ajaxLoadingForNewQuestions = false;
					});

				},

				deleteQuestion : function (id,leadReactor) {
					var self = this;
//					alert('In progress')

					if(leadReactor==1)
					{
						swal({
							title: "Warning!!! This is a campaign",
							text: "All associated leads attach to this question will be also removed",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Confirm!",
							closeOnConfirm: false,
							showLoaderOnConfirm: false
						}, function () {
							confirm('/admin/question/'+id+'/delete',function (response) {
								self.designations = response.data;
							});
						});
					}
					else {
						confirm('/admin/question/'+id+'/delete',function (response) {
							self.designations = response.data;
						});
					}



				},

				attachCoreg : function(coreg){
					this.currentCoregId = coreg.id;
					$('#newCoregImage').attr('src','/'+coreg.image);
					$('#newCoregDeal').text(coreg.deal);
					$('#newCoregDescription').text(coreg.description);
					$('#newCoregDealInput').val(coreg.deal);
					$('#newCoregDescriptionInput').val(coreg.description);


					$('#attachCoregQuestionsModal').modal('hide');
					$('#coregConfirmationModal').modal('show');
				},

				cancelCoregConfirmation : function () {
					$('#attachCoregQuestionsModal').modal('show');
					$('#coregConfirmationModal').modal('hide');
				},

				saveCoregConfirmation : function () {

					var self = this;

					self.ajaxLoadingForNewQuestions = true;

					var data = $('#newCoregQuestionForm').serializeArray();
					data.push({ name: 'designation_id', value: this.currentDesignationId });
					data.push({ name: 'campaign_id', value: this.currentCoregId });
					data.push({ name: 'lead_reactor', value: 1 });
					data.push({ name: 'polar', value: 1 });

					$.ajax({
						url:'/admin/question/store',
						method:'POST',
						data: data
					}).success(function(response) {
						notify("Success!", "Coreg has been inserted", "success");
						$('#newCoregQuestionForm').find("input[type=text], textarea").val("");
						self.designations = response.data;
						self.getCampaignPaginate();
					}).fail(function(e) {
						notify(e.statusText || ($.parseJSON(e.responseText)).message, "", "error", 'overlay');
					}).done(function(){
						self.ajaxLoadingForNewQuestions = false;
					});

				},

				attachLinkout : function(offer)
				{
					var self = this;

					var data = [];
					data.push({ name: 'designation_id', value: this.currentDesignationId });
					data.push({ name: 'title', value: offer.name });
					data.push({ name: 'link_out', value: 1 });
					data.push({ name: 'offer_id', value: offer.id });

					$.ajax({
						url:'/admin/question/store',
						method:'POST',
						data: data
					}).success(function(response) {
						notify("Success!", "Offer has been inserted", "success");
						self.designations = response.data;
					}).fail(function(e) {
						notify(e.statusText || ($.parseJSON(e.responseText)).message, "", "error", 'overlay');
					}).done(function(){
						self.ajaxLoadingForNewQuestions = false;
					});

				},

				updateDesignation : function (e, id) {
					var self = this;
					self.ajaxLoadingForUpdateDesignation = id;

					var $this = $(e.target);

					var data = {
						header_big : $($this).closest('.row').find('.designation-title').val(),
						header_small : $($this).closest('.row').find('.designation-title-small').val(),
						no_of_questions : $($this).closest('.row').find('.designation-max').val(),
						id : id
					};

					$.ajax({
						url:'/admin/designation/update',
						method:'PATCH',
						data: data
					}).success(function(response) {
						notify("Success!", "Coreg has been inserted", "success");
						console.log(response);
						self.designations = response.data;
					}).fail(function(e) {
						notify(e.statusText || ($.parseJSON(e.responseText)).message, "", "error", 'overlay');
					}).done(function(){
						self.ajaxLoadingForUpdateDesignation = 0;
					});
				}
			},

			filters : {

				paid : function(data)
				{
					if(data!=null || data>0)
					{
						return 'Yes';
					}
					return 'No';
				},

				coregFilter : function(data)
				{
					if(data.lead_reactor==1)
					{
						return 'Coreg';
					}
					else if(data.link_out==1)
					{
						return 'Linkouts';
					}
					else
					{
						return 'Normal';
					}

				},

				imageFormatter : function(data)
				{
					return '/'+data;
				},

				offerFilter : function(data)
				{
					if(data==1)
					{
						return 'Sponsor';
					}
					return 'Freebies';
				},

				hpcsFilter : function (data)
				{
					if(data==1)
					{
						return 'Yes';
					}
					return 'No';
				}

			}

		});



















//			$('#surveys-table').DataTable({
//			processing: true,
//			serverSide: true,
//			responsive: true,
//			ajax: '/admin/survey/data',
//			columns: [
//				{ data: 'id', name: 'id' },
//				{ data: 'title', name: 'title' },
//				{ data: 'label', name: 'label' },
//				{ data: 'order', name: 'order' },
//				{ data: 'lead_reactor', name: 'lead_reactor' },
//				{ data: 'lead_reactor', name: 'lead_reactor' },
//				{ data: 'lead_reactor', name: 'lead_reactor' },
//				{ data: 'lead_reactor', name: 'lead_reactor' },
//				{ data: 'lead_reactor', name: 'lead_reactor' },
//				{ data: 'action','className': 'text-center', name: 'action', orderable: false,searchable: false, 'width': '15%' },
//			],
////			"oLanguage": {
////				"sLengthMenu": "Entries per page: _MENU_",
////			},
//		});
//		$('<button id="refresh" type="button" class="btn btn-default"> <i class="fa fa-refresh" aria-hidden="true"></i> Refresh</button>').appendTo('div.dataTables_filter');
//
//		$('#refresh').click(function(){
//			$('#users-table').dataTable().api().draw();
//		});
	</script>
@stop