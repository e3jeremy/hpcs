@extends('admin.layout.main', ['title' => 'Reports','description' => 'Logs and Reports'])
@section('title', 'Dashboard')
@section('content')


    <div class="portlet light portlet-fit ">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-people font-dark"></i>
                <span class="caption-subject font-dark bold uppercase">Logs and Reports Statistics</span>
            </div>
            <div class="actions col-md-3">

            </div>
        </div>
        <div class="portlet-body">

            <reports></reports>

        </div>
    </div>

@stop