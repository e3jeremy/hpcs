<div class="page-footer">
    <div class="page-footer-inner"> 2016 © 
        <a href="/">HighPayingCashSurveys.com</a> 
        All Rights Reserved
        <div class="scroll-to-top" style="display: block;">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <!-- END FOOTER -->
</div>