@extends('admin.layout.main', ['title' => 'Local','description' => 'External Coregs / Local Surveys'])
@section('title', 'Dashboard')
@section('content')


    <div class="portlet light portlet-fit ">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-link font-dark"></i>
                <span class="caption-subject font-dark bold uppercase">Local Survey</span>
            </div>
            <div class="actions col-md-3">
                <div class="input-group pull-right">
                    <input v-model="searchField" type="text" class="form-control" placeholder="Search for..." @keyup="searchFor('offerSearch',$event) | debounce 500">
                                <span class="input-group-btn">
                                <button class="btn btn-default" @click="searchFor('offerSearch',$event)" type="button">Go!</button>
                                </span>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <local></local>
        </div>
    </div>

@stop