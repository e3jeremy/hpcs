@extends('admin.layout.main', ['title' => 'Dashboard','description' => 'Home'])
@section('title', 'Dashboard')
@section('content')

    <div class="portlet light portlet-fit ">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-people font-dark"></i>
                <span class="caption-subject font-dark bold uppercase">Dashboard</span>
            </div>
        </div>
        <div class="portlet-body">
            <dashboard></dashboard>
        </div>
    </div>
@stop

@push('scripts')
    <script src="/js/backend/raphael.min.js"></script>
    <script src="/js/backend/morris.min.js"></script>
@endpush
