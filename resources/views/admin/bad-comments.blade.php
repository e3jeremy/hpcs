@extends('admin.layout.main', ['title' => 'Bad Comments','description' => 'Bad Comments Users Feedback'])

@section('content')

    <div class="portlet light portlet-fit ">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-people font-dark"></i>
                <span class="caption-subject font-dark bold uppercase">Bad Comments</span>
            </div>
            <div class="actions col-md-3">
                <div class="input-group pull-right">
                    <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                                </span>
                </div>
            </div>
        </div>
        <div class="portlet-body">

            <bad-comments></bad-comments>

        </div>
    </div>

@stop