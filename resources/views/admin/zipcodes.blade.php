@extends('admin.layout.main', ['title' => 'Zipcodes','description' => 'Zipcodes'])
@section('title', 'Dashboard')
@section('content')

    <div class="portlet light portlet-fit ">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-people font-dark"></i>
                <span class="caption-subject font-dark bold uppercase">Zipcodes</span>
            </div>
            <div class="actions col-md-3">
                <div class="input-group pull-right">
                    <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                                </span>
                </div>
            </div>
        </div>
        <div class="portlet-body">

            <zip-codes></zip-codes>

        </div>
    </div>


@stop