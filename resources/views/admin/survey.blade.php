@extends('admin.layout.main', ['title' => 'Survey','description' => 'Survey Manager'])
@section('title', 'Dashboard')
@section('content')


    <div class="portlet light portlet-fit ">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-people font-dark"></i>
                <span class="caption-subject font-dark bold uppercase">Survey</span>
            </div>
        </div>
        <div class="portlet-body">

            <survey></survey>

        </div>
    </div>

@stop