<html>
<body>

<strong>A customer has filled out the online form on our website. Here are the details:</strong>
<hr>
<div>
    <table>
        <tr>
            <td width="15%">Name :</td>
            <td width="85%"> {{ $name }}</td>
        </tr>

        <tr>
            <td>Email :</td>
            <td> {{ $email_address }}</td>
        </tr>

        <tr>
            <td>City :</td>
            <td>{{ $city }}</td>
        </tr>
        <tr>
            <td> State :</td>
            <td>{{ $state }}</td>
        </tr>
        <tr>
            <td> Zip :</td>
            <td>{{ $zip }}</td>
        </tr>
        <tr>
            <td> IP :</td>
            <td> {{ $ip }}</td>
        </tr>
        <tr>
            <td> Add Code :</td>
            <td> {{ $add_code }}</td>
        </tr>
        <tr>
            <td> Last Visited :</td>
            <td> Page-{{ $page }}</td>
        </tr>
        <tr>
            <td> Url :</td>
            <td> {{ $url }}</td>
        </tr>
        <tr>
            <td> Agent :</td>
            <td> {{ $agent }}</td>
        </tr>
        <tr>
            <td> Category :</td>
            <td> {{ $topic }}</td>
        </tr>
        <tr>
            <td> Comments :</td>
            <td> {{ $feedback }}</td>
        </tr>


    </table>
</div>

</body>
</html>