@extends('frontend.layouts.main3')

@section('links')
    <style>
        #navigation{
            display: none !important;
        }
    </style>
@endsection

@section('content')

    <form id="reset-pass">

        <div class="row" id="app">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-primary">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Re create your new password</div>
                        <div class="panel-body">
                            <h4 class="modal-title"><img src="/images/logo.png" alt="hpcs-logo" class="img-responsive"></h4>
                            <br>

                            <div class="form-group">
                                <label for="">Enter Your New password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="New Password">
                            </div>
                            <div class="form-group">
                                <label for="">Confirm password</label>
                                <input type="password" class="form-control" id="confirm_password" name="password_confirmation" placeholder="Confirm Password">
                                <input type="hidden" class="form-control" id="remember_token" name="remember_token" value="{{ $token }}">
                                <input type="hidden" class="form-control" id="_token" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" class="form-control" id="_method" name="_method" value="PATCH">

                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" id="saveNewPassword" class="btn btn-primary form-control">Update your New Password</button>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{ URL::to('/') }}" class="btn btn-success form-control">Proceed to login</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

    </form>

@stop

@section('scripts')
    <script type="text/javascript">

        $('#saveNewPassword').click(function(){
            var password = $('#password').val();
            var c_password = $('#confirm_password').val();

            if(c_password.length < 6 || password.length < 6)
            {
                notify("Min of 6 characters required", "", "error", 'overlay');
                return false
            }


            if(password!=c_password)
            {
                notify("Passwords dont match", "", "error", 'overlay');
                return false;
            }
            else
            {
                $(this).text('Sending Request');
                $(this).attr('disabled',true);

                $.ajax({
                    url:'/reset/password/update',
                    method:'PATCH',
                    data: $('#reset-pass').serialize()
                }).success(function(response) {
                    if(response.code==200)
                    {
                        notify("Password reset successful! Please Login now", "", "success");
                        window.location = window.location.origin;
                    }
                }).fail(function(e) {
                    notify(e.statusText || ($.parseJSON(e.responseText)).message, "", "error", 'overlay');
                    $(this).text('Update New Password');
                    $(this).attr('disabled',false);
                }).done(function(){
                });
            }
        })

    </script>


@endsection