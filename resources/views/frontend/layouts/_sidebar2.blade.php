<div class="col-md-4">
	<div class="challenge">
		<div class="panel panel-default">
			<div class="panel-heading text-center">30 Day Challenge</div>
				<div class="panel-body">
					<div class="day-container text-center">
						<div class="done">$1</div>
						@for ($i = 1; $i <= 29; $i++)
	           <div class="ongoing">$1</div>
	          @endfor
					</div>
				</div>
		</div>
	</div>
	<div class="bonus">
		<div class="panel panel-default">
			<div class="panel-heading text-center">Bonus task</div>
				<div class="panel-body">
						<div class="social-container">
							<div class="text-center">
								<a href="" title="twitter"><img src="/images/icons/twitter.png" alt="twitter"></a>
								<a href="" title="youtube"><img src="/images/icons/youtube.png" alt="youtube"></a>
								<a href="" title="facebook"><img src="/images/icons/facebook.png" alt="facebook"></a>
								<a href="" title="linkedin"><img src="/images/icons/linkedin.png" alt="linkedin"></a>
								<a href="" title="pinterest"><img src="/images/icons/pinterest.png" alt="pinterest"></a>
								<a href="" title="flickr"><img src="/images/icons/flickr.png" alt="flickr"></a>
								<a href="" title="share"><img src="/images/icons/share.png" alt="share"></a>
								<a href="" title="rss"><img src="/images/icons/rss.png" alt="rss"></a>
								<p>like us on facebook and get $1!</p>
							</div>
						</div>
				</div>
		</div>
	</div>
	@include('frontend.layouts._faq')
</div>