 <!-- Accordion -->
<div class="footer-accordion panel-group accordion  animated" style="display:none" transition="bounce" v-show="faq">
    <div class="panel panel-default">
        <button type="button" class="btn btn-faq" @click="faq=!faq">FAQ</button>
        <div class="panel-heading">
            <h4 class="panel-title">
            <i class="indicator fa fa-plus-circle pull-left" aria-hidden="true" data-toggle="collapse" data-parent="#accordion" href="#collapseHow"></i>
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseHow">How it works?</a>
            </h4>
        </div>
        <div id="collapseHow" class="panel-collapse collapse">
            <div class="panel-body">
                <p>Market research companies pay us to offer you wuth surveys for your opinions. They pay us, you get to take surveys for free. Even better, you can get reward with cash, prizes and products.</p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
            <i class="indicator fa fa-plus-circle pull-left" aria-hidden="true" data-toggle="collapse" data-parent="#accordion" href="#collapseGoal"></i>
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseGoal">Our Goals</a>
            </h4>
        </div>
        <div id="collapseGoal" class="panel-collapse collapse">
            <div class="panel-body">
                <p>Our aim is to build a community of survey panelists who rely on us for surveys that offer great rewards. We want your experience to be enjoyable and rewarding!</p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
            <i class="indicator fa fa-plus-circle pull-left" aria-hidden="true" data-toggle="collapse" data-parent="#accordion" href="#collapsePaid"></i>
            <a data-toggle="collapse" data-parent="#accordion" href="#collapsePaid">How do I get paid?</a>
            </h4>
        </div>
        <div id="collapsePaid" class="panel-collapse collapse">
            <div class="panel-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptas soluta reprehenderit quos nulla dolor perspiciatis, explicabo, quod sint et.</p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
            <i class="indicator fa fa-plus-circle pull-left" aria-hidden="true" data-toggle="collapse" data-parent="#accordion" href="#collapseFaq"></i>
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFaq">What if you are disqualified from a survey?</a>
            </h4>
        </div>
        <div id="collapseFaq" class="panel-collapse collapse">
            <div class="panel-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptas soluta reprehenderit quos nulla dolor perspiciatis, explicabo, quod sint et.</p>
            </div>
        </div>
    </div>
    </div> <!-- end #accordion -->