<div class="spacer visible-xs"></div>
<div class="col-md-4 sidebar-one">
	<div class="row">
		<div class="col-xs-6 hidden-sm hidden-lg">
			<img src="/images/promise.png" class="mobile-promise" alt="logo"> <!-- show only on mobile -->
		</div>
		<div class="col-xs-6 col-md-12 hidden-xs hidden-md">
			<img src="/images/promise.png" class="large-promise" alt="logo"> <!-- show only on large screen -->
		</div>
		<div class="col-xs-6 col-md-12">
			<!-- Description -->
			<div class="illustration">
				<p>
					Go through the 30 Day Challenge. See how simple it is to make $30 by giving your opinions? If, after 
					30 days, you are unsatisfied with the results, send us an email. Explain why our simple process failed to meet'
					our expectations and we'll pay you $10.
				</p>
			</div>
		</div>
	</div>
</div>

<!-- sidebar -->