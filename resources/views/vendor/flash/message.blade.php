@if (session()->has('flash_notification.message'))
    @if (session()->has('flash_notification.overlay'))
        <script>
            $(document).ready(function() {
                swal({   title: "{{session('flash_notification.title')}}",   text: "{{session('flash_notification.message')}}",   type: "{{session('flash_notification.level')}}"});
            });
        </script>
    @else
        <script>
            $(document).ready(function () {
                swal({   title: "{{session('flash_notification.title')}}",   text: "{{session('flash_notification.message')}}",   timer: 3000, showConfirmButton : false,   type: "{{session('flash_notification.level')}}" });
            });
        </script>
    @endif
@endif
