
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>HPCS | Dashboard</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="_token" content="H0YVOE3ZtTjJ9WkEocPywOjJaJ2bVJgf96PtK6ma">

    <!-- Begin mandatory styles -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">


    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.min.css">
    <!-- End mandatory styles -->
    <!-- BEGIN GLOBAL STYLES -->
    <link rel="stylesheet" type="text/css" href="/assets/admin/css/component.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/admin/css/plugin.min.css">
    <link rel="stylesheet" type="text/css" href="http://vadimsva.github.io/waitMe/waitMe.min.css">
    <!-- END GLOBAL STYLES -->
    <!-- BEGIN MAIN LAYOUT STYLES -->
    <link rel="stylesheet" type="text/css" href="/assets/admin/css/layout.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/admin/css/style.min.css">
    <!-- END MAIN LAYOUT STYLES -->
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="/">
                <img src="/assets/admin/images/logo.png" alt="logo" class="logo-default"/>
            </a>
            <div class="menu-toggler sidebar-toggler">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN PAGE TOP -->
        <div class="page-top">
            <!-- BEGIN HEADER SEARCH BOX -->
            <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
            <form class="search-form search-form-expanded" action="page_general_search_3.html" method="GET">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search survey.." name="query">
                    <span class="input-group-btn">
                        <a href="javascript:;" class="btn submit">
                            <i class="icon-magnifier"></i>
                        </a>
                    </span>
                </div>
            </form>
            <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN NOTIFICATION DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class below "dropdown-extended" to change the dropdown styte -->
                    <!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
                    <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->

                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img alt="" class="img-circle" src="/assets/admin/images/avatar.png" />
                            <span class="username username-hide-on-mobile"> Admin </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="http://hpcstest.engageiq.com/admin/v2/profile">
                                    <i class="icon-user"></i> My Profile </a>
                            </li>
                            <li>
                                <a href="app_calendar.html">
                                    <i class="icon-calendar"></i> My Calendar </a>
                            </li>
                            <li>
                                <a href="app_inbox.html">
                                    <i class="icon-envelope-open"></i> My Inbox
                                    <span class="badge badge-danger"> 3 </span>
                                </a>
                            </li>
                            <li>
                                <a href="app_todo_2.html">
                                    <i class="icon-rocket"></i> My Tasks
                                    <span class="badge badge-success"> 7 </span>
                                </a>
                            </li>
                            <li class="divider"> </li>
                            <li>
                                <a href="page_user_lock_1.html">
                                    <i class="icon-lock"></i> Lock Screen </a>
                            </li>
                            <li>
                                <a href="/admin/logout">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                    <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-extended quick-sidebar-toggler">
                        <span class="sr-only">Toggle Quick Sidebar</span>
                        <i class="icon-logout"></i>
                    </li>
                    <!-- END QUICK SIDEBAR TOGGLER -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->

<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <!-- END SIDEBAR -->
        <div class="page-sidebar navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
            <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
            <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
            <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="false" data-slide-speed="200">
                <li @click="loadingPage" class="nav-item active">
                <a href="http://hpcstest.engageiq.com/admin/v2/dashboard" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                </li>
                <li @click="loadingPage" class="nav-item  ">
                <a href="http://hpcstest.engageiq.com/admin/v2/survey" class="nav-link nav-toggle">
                    <i class="icon-list"></i>
                    <span class="title">Surveys</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                </li>
                <li @click="loadingPage" class="nav-item  ">
                <a href="http://hpcstest.engageiq.com/admin/v2/survey/questions" class="nav-link nav-toggle">
                    <i class="icon-question"></i>
                    <span class="title">Survey Question</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                </li>
                <li @click="loadingPage" class="nav-item  ">
                <a href="http://hpcstest.engageiq.com/admin/v2/coregs" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">Coregs Campaign</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                </li>
                <li @click="loadingPage" class="nav-item  ">
                <a href="http://hpcstest.engageiq.com/admin/v2/local" class="nav-link nav-toggle">
                    <i class="icon-link"></i>
                    <span class="title">Local Surveys</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                </li>
                <li @click="loadingPage" class="nav-item  ">
                <a href="http://hpcstest.engageiq.com/admin/v2/users" class="nav-link nav-toggle">
                    <i class="icon-people"></i>
                    <span class="title">Users</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                </li>
                <li @click="loadingPage" class="nav-item  ">
                <a href="http://hpcstest.engageiq.com/admin/v2/zipcodes" class="nav-link nav-toggle">
                    <i class="icon-puzzle"></i>
                    <span class="title">Zip Codes</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                </li>
                <li @click="loadingPage" class="nav-item  ">
                <a href="http://hpcstest.engageiq.com/admin/v2/good/comments" class="nav-link nav-toggle">
                    <i class="icon-briefcase"></i>
                    <span class="title">Good Comments</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                </li>
                <li @click="loadingPage" class="nav-item  ">
                <a href="http://hpcstest.engageiq.com/admin/v2/feedback" class="nav-link nav-toggle">
                    <i class="icon-bubbles"></i>
                    <span class="title">Feedback & Opinions</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                </li>
                <li @click="loadingPage" class="nav-item  ">
                <a href="http://hpcstest.engageiq.com/admin/v2/bad/comments" class="nav-link nav-toggle">
                    <i class="icon-docs"></i>
                    <span class="title">Bad Comments</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                </li>
                <li @click="loadingPage" class="nav-item  ">
                <a href="http://hpcstest.engageiq.com/admin/v2/bad/comments/filter" class="nav-link nav-toggle">
                    <i class="icon-tag"></i>
                    <span class="title">Bad Comments Filter</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                </li>
            </ul>
            </li>
            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
        <!-- END SIDEBAR -->    </div>
    <div class="page-content-wrapper" id="page-loading">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content" id="page-wrapper">
            <h1 class="page-title"> Dashboard
                <small>Home</small>
            </h1>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <span>Dashboard</span>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12" id="page-content">

                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-people font-dark"></i>
                                <span class="caption-subject font-dark bold uppercase">Dashboard</span>
                            </div>
                            <div class="actions col-md-3">
                                <div class="input-group pull-right">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                                </span>
                                </div>
                            </div>
                        </div>asdas
                        saddfsd
                        ssdasd
                        sd

                        sd
                        ads



                        sdasdsadas
                        dsdsda
                        as
                        sdsd
                        aasaaa
                        aa
                        aaa

                        sdsd
                        ad
                        dsdsds

                        sd
                        ssa
                        ss="portlet-body">

                            <dashboard></dashboard>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="page-footer">
        <div class="page-footer-inner"> 2016 ©
            <a href="/">HighPayingCashSurveys.com</a>
            All Rights Reserved
            <div class="scroll-to-top" style="display: block;">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
    </div>	<!-- BEGIN CORE PLUGINS -->
    <script src="/assets/admin/js/jquery.min.js" type="text/javascript"></script>
    <script src="/assets/admin/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/assets/admin/js/cookie.min.js" type="text/javascript"></script>
    <script src="/assets/admin/js/slimscroll.min.js" type="text/javascript"></script>
    <script src="/assets/admin/js/blockui.min.js" type="text/javascript"></script>
    <script src="/assets/admin/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="/assets/admin/js/app.min.js" type="text/javascript"></script>
    <script src="http://vadimsva.github.io/waitMe/waitMe.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <!-- <script src="/assets/admin/js/dashboard.min.js" type="text/javascript"></script> -->
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="/assets/admin/js/layout.min.js" type="text/javascript"></script>
    <script src="/assets/admin/js/demo.min.js" type="text/javascript"></script>
    <script src="/assets/admin/js/quick-sidebar.min.js" type="text/javascript"></script>
    <script src="/assets/admin/js/quick-nav.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    <script>
        function startLoading() {
            $('#page-loading').waitMe({
                effect : 'rotateplane',
                text : 'Please wait',
                bg : 'rgba(255,255,255,0.1)',
                color : '#000'
            });
        }
        function stopLoading() {
            $('#page-loading').waitMe("hide");
        }
    </script>

    <script src="/js/dropzone.js"></script>
    <script src="/js/backend/common.js"></script>
    <script src="/js/backend/main.js"></script>


    <!-- END THEME LAYOUT SCRIPTS -->
</body>
</html>