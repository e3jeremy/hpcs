<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>High Paying Cash Surveys</title>
	<!-- Begin Favicons for html and mobile browsers -->
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/images/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/images/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">

    <link rel="mask-icon" href="/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="apple-mobile-web-app-title" content="HighPayingCashSurveys">
    <meta name="application-name" content="HighPayingCashSurveys">
    <meta name="theme-color" content="#ffffff">
    <!-- End Favicons for html and mobile browsers -->

    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}" id="main">
    <style type="text/css" media="screen">
    	.error-template {padding: 50px 15px;text-align: center;}
    	h1 {color: #00CCFF;}
    	h2 {color: #FF9933;}
    	.error-details {margin-bottom: 15px;}
        .error-actions {margin-top:20px;margin-bottom:15px;}
        .btn { margin-right:10px; margin-bottom:5px }
    </style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			    <div class="error-template">
			        <h1>Oops!</h1>
			        <h2>404. Page Not Found.</h2>
			        <div class="error-details">
			            Unfortunately, the page you are looking for doesn't exist!
			        </div>
			        <div>
			            <a id="goHome" href="#" class="btn btn-primary"><span class="glyphicon glyphicon-home"></span>
			                Take Me Home </a>
		                <a href="mailto:francis@engageiq.com" class="btn btn-default"><span class="glyphicon glyphicon-envelope"></span> Contact Support </a>
			        </div>
			    </div>
			</div>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script>
		$(function(){
			$('#goHome').click(function(){
				document.location.href = window.location.protocol + "//" + window.location.host + "/";
			})
		})
	</script>
</body>
</html>