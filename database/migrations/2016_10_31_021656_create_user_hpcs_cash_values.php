<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserHpcsCashValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('hpcs_taken')->nullable();
            $table->text('offers_taken')->nullable();
            $table->text('freebies_taken')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['hpcs_taken']);
            $table->dropColumn(['offers_taken']);
            $table->dropColumn(['freebies_taken']);
        });
    }
}
