<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderingOffers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->integer('order')->unique()->nullable();
            $table->integer('up_to')->nullable();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('mail_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn(['order']);
            $table->dropColumn(['up_to']);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['mail_address']);
        });
    }
}
