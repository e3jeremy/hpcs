<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLinksColumnToOffers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->renameColumn('id', 'id_stnk');
            $table->boolean('click_paid')->default(0);
            $table->text('link')->nullable();
            $table->boolean('has_sweeptakes');
            $table->boolean('sweeptakes');
            $table->boolean('has_coupons');
            $table->boolean('coupons');
//            $table->increments('id');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn(['coupons']);
            $table->dropColumn(['creative_id']);
            $table->renameColumn('id_stnk', 'id');
            $table->dropColumn(['link']);
            $table->dropColumn(['has_sweeptakes']);
            $table->dropColumn(['sweeptakes']);
//            $table->dropColumn('id');
        });
    }
}
