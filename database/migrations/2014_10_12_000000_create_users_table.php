<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->char('zip',5)->nullable();
            $table->char('ip',15)->nullable();
            $table->date('dob')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            $table->char('gender',1)->nullable();
            $table->boolean('paypal')->default(1);
            $table->boolean('registered')->default(0);
            $table->boolean('challenge_notification')->default(1);
            $table->date('last_challenge')->nullable();
            $table->decimal('hpcs',8,2)->default(0);
            $table->decimal('challenge',8,2)->default(0);
            $table->decimal('sponsors',9,2)->default(0);
            $table->boolean('can_challenge')->default(0);
            $table->smallInteger('hpcs_designation')->default(1)->unsigned();
            $table->smallInteger('challenge_designation')->default(1)->unsigned();
            $table->tinyInteger('offer_progress')->default(0);
            $table->text('offers')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
