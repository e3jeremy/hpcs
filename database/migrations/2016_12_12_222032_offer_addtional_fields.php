<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferAddtionalFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->string('additional_fields')->nullable();
        });

        Schema::table('coreg_leads', function (Blueprint $table) {
            $table->string('additional_fields')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropColumn(['additional_fields']);
        });

        Schema::table('coreg_leads', function (Blueprint $table) {
            $table->dropColumn(['additional_fields']);
        });
    }
}
