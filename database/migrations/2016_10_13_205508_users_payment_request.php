<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersPaymentRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_request', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->string('type')->nullable();
            $table->decimal('challenge',5,2)->default(0);
            $table->smallInteger('status')->default(0);
            $table->string('transaction_type')->nullable();
            $table->string('transaction_number')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::drop('users_request');
    }
}
