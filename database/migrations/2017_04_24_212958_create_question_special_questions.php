<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionSpecialQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->integer('special')->default(0);

        });

        Schema::table('users', function (Blueprint $table) {
            $table->boolean('pp_tos')->default(0);

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->dropColumn(['special']);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['pp_tos']);
        });
    }
}