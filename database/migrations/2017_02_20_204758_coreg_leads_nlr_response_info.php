<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CoregLeadsNlrResponseInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coreg_leads', function (Blueprint $table) {
            $table->text('nlr_submitted')->nullable();
            $table->text('nlr_response')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coreg_leads', function (Blueprint $table) {
            $table->dropColumn(['nlr_submitted']);
            $table->dropColumn(['nlr_response']);
        });
    }
}
