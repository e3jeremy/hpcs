<?php

use App\HPCS\Entities\Category;
use App\HPCS\Entities\Offer;
use Illuminate\Database\Seeder;

class CampaignSeeder3 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(['display' => 'All', 'type' => 'all']);
        Category::create(['display' => 'Cash', 'type' => 'cash']);
        Category::create(['display' => 'Points', 'type' => 'points']);
        Category::create(['display' => 'Sweepstakes', 'type' => 'sweepstakes']);

        $offer = Offer::create([
            'name' => 'Inbox Pays',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 284,
            'link' => 'http://engageiq.nlrtrk.com/?a=7746&c=284&s1=&s1=1862&s2=277&s3=2'
        ]);

        $offer->categories()->sync([1,2,3,4]);


        $offer = Offer::create([
            'name' => 'Freebies Frenzy',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 350,
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'name' => 'Jobs to Shop',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 13,
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'name' => 'Sample Quality Health 2',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 78,
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'name' => 'New Panda',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 285,
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'name' => 'Toluna',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 25,
        ]);

        $offer->categories()->sync([1,2]);

        $offer = Offer::create([
            'name' => 'Cashback Clik',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 19,
        ]);

        $offer->categories()->sync([1,3]);

        $offer = Offer::create([
            'name' => 'My Survey',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 15,
        ]);

        $offer->categories()->sync([1,4]);

        $offer = Offer::create([
            'name' => 'Pepsi vs Coke',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 397,
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'name' => 'Valued Opinions',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 17,
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'name' => 'Nielsen NDV',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 80,
        ]);

        $offer->categories()->sync([1,2,3,4]);

        $offer = Offer::create([
            'name' => 'Earning Station',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 405,
        ]);

        $offer->categories()->sync([1,2,3,4]);

        $offer = Offer::create([
            'name' => 'E Poll',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 18,
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'name' => 'Survey Spot',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 348,
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'name' => 'Diabetes Quality Health 2',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 435,
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'name' => 'Pro Opinion',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 474,
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'name' => 'Harris Poll',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 45,
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'name' => 'Nielsen Tablet',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 395,
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'name' => 'American Consumer Opinion',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 28,
        ]);

        $offer->categories()->sync([1,2]);

        $offer = Offer::create([
            'name' => 'Mind\'s Pay',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 283,
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'name' => 'Vip Voice',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 24,
        ]);

        $offer->categories()->sync([1,3]);

        $offer = Offer::create([
            'name' => 'Cada Cabeza',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 473,
        ]);

        $offer->categories()->sync([1,4]);

        $offer = Offer::create([
            'name' => 'Survey Downline',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 49,
        ]);

        $offer->categories()->sync([1,2,4]);

        $offer = Offer::create([
            'name' => 'Cash Crate',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 84,
        ]);

        $offer->categories()->sync([1,4,3]);

        $offer = Offer::create([
            'name' => 'Real Age',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 372,
        ]);

        $offer->categories()->sync([1,4,3]);


        $offer = Offer::create([
            'name' => 'Fusion Cash',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'campaign_id' => 21,
        ]);

        $offer->categories()->sync([1,2,4]);

    }
}
