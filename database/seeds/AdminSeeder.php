<?php

use App\HPCS\Entities\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'email' => 'admin@hpcs.com',
            'password' => bcrypt('password'),
            'first_name' => 'Admin',
            'last_name' => 'HPCS'
        ]);

        Admin::create([
            'email' => 'highpayingcashsurveysv2@gmail.com',
            'password' => bcrypt('password'),
            'first_name' => 'email',
            'last_name' => 'HPCS'
        ]);
    }
}
