<?php

use App\HPCS\Entities\Category;
use App\HPCS\Entities\Offer;
use Illuminate\Database\Seeder;

class CampaignSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(['display' => 'All', 'type' => 'all']);
        Category::create(['display' => 'Cash', 'type' => 'cash']);
        Category::create(['display' => 'Points', 'type' => 'points']);
        Category::create(['display' => 'Sweepstakes', 'type' => 'sweepstakes']);

        $offer = Offer::create([
            'image' => '/offers/new_inboxpays.png',
            'name' => 'Inbox Pays',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 284
        ]);

        $offer->categories()->sync([1,2,3,4]);

        $offer = Offer::create([
            'image' => '/offers/cpawall_freebiesfrenzy.png',
            'name' => 'Freebies Frenzy',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 350
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'image' => '/offers/new_jobs2shop.png',
            'name' => 'Jobs to Shop',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 13
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'image' => '/offers/cpawall_sample_quality_health2.png',
            'name' => 'Sample Quality Health 2',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 78
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'image' => '/offers/new_panda.png',
            'name' => 'New Panda',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 285
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'image' => '/offers/new_toluna.png',
            'name' => 'Toluna',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 25
        ]);

        $offer->categories()->sync([1,2]);

        $offer = Offer::create([
            'image' => '/offers/cpawall_cashbackclik.png',
            'name' => 'Cashback Clik',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 19
        ]);

        $offer->categories()->sync([1,3]);

        $offer = Offer::create([
            'image' => '/offers/new_mysurvey.png',
            'name' => 'My Survey',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 15
        ]);

        $offer->categories()->sync([1,4]);

        $offer = Offer::create([
            'image' => '/offers/cpawall_pepsi_vs_coke.png',
            'name' => 'Pepsi vs Coke',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 397
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'image' => '/offers/new_valuedopinions.png',
            'name' => 'Valued Opinions',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 17
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'image' => '/offers/cpawall_nielsen_ndv.png',
            'name' => 'Nielsen NDV',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 80
        ]);

        $offer->categories()->sync([1,2,3,4]);

        $offer = Offer::create([
            'image' => '/offers/cpawall_earningstation.png',
            'name' => 'Earning Station',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 405
        ]);

        $offer->categories()->sync([1,2,3,4]);

        $offer = Offer::create([
            'image' => '/offers/cpawall_epoll.png',
            'name' => 'E Poll',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 18
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'image' => '/offers/new_surveyspot.png',
            'name' => 'Survey Spot',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 348
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'image' => '/offers/cpawall_diabetes_quality_health2.png',
            'name' => 'Diabetes Quality Health 2',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 435
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'image' => '/offers/cpawall_pro_opinion.png',
            'name' => 'Pro Opinion',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 474
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'image' => '/offers/cpawall_harrispoll.png',
            'name' => 'Harris Poll',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 45
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'image' => '/offers/nielsen_tablet.png',
            'name' => 'Nielsen Tablet',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 395
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'image' => '/offers/cpawall_acop.png',
            'name' => 'American Consumer Opinion',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 28
        ]);

        $offer->categories()->sync([1,2]);

        $offer = Offer::create([
            'image' => '/offers/new_mindspay.png',
            'name' => 'Mind\'s Pay',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 283
        ]);

        $offer->categories()->sync([1,2,3]);

        $offer = Offer::create([
            'image' => '/offers/cpawall_vipvoice.png',
            'name' => 'Vip Voice',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 24,
        ]);

        $offer->categories()->sync([1,3]);

        $offer = Offer::create([
            'image' => '/offers/cpawall_cada_cabeza.png',
            'name' => 'Cada Cabeza',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 473,
        ]);

        $offer->categories()->sync([1,4]);

        $offer = Offer::create([
            'image' => '/offers/survey_downline.png',
            'name' => 'Survey Downline',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 49,
        ]);

        $offer->categories()->sync([1,2,4]);

        $offer = Offer::create([
            'image' => '/offers/cash_crate.png',
            'name' => 'Cash Crate',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 84,
        ]);

        $offer->categories()->sync([1,4,3]);

        $offer = Offer::create([
            'image' => '/offers/cpawall_realage.png',
            'name' => 'Real Age',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 372,
        ]);

        $offer->categories()->sync([1,4,3]);


        $offer = Offer::create([
            'image' => '/offers/new_fusioncash.png',
            'name' => 'Fusion Cash',
            'subtitle' => 'Subtitle goes here',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis earum a sed repellendus illo maiores quo ullam beatae adipisci reiciendis.',
            'cash' => 5,
            'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
            'id' => 21,
        ]);

        $offer->categories()->sync([1,2,4]);

    }
}
